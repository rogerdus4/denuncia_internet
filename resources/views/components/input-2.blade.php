<div class="col-md-2">
    <div class="form-group">
        <div class="form-line">
            {{ $slot }}
        </div>
    </div>
</div>
