<div class="col-md-12">
    <div class="form-group">
        <div class="form-line">
            {{ $slot }}
        </div>
    </div>
</div>
