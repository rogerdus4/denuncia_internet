<div class="col-md-1">
    <div class="form-group">
        <div class="form-line">
            {{ $slot }}
        </div>
    </div>
</div>
