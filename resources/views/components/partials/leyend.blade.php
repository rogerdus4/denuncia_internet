<div>
    <div class="header text-justify">
        <h2>El presente módulo tiene por objeto obtener una constancia de extravío de objetos o documentos.</h2>
        <h2>
            <span class="font-bold">No se admitirá </span> presentación de denuncias cuando se refiera al <span
                class="font-bold"> robo o extravió de tarjetas de
                crédito, débito </span> o de servicios emitidas por instituciones que integran el sistema financiero, ni
            en los casos de títulos de crédito,<span class="font-bold"> así como tampoco por el robo de vehículos </span>.
            Para la
            presentación de denuncias en estos
            casos deberá dar clic <a href="http://fgjem.edomex.gob.mx/pre-denuncia-en-linea" TARGET="_new"><span
                    class="font-bold col-cyan">aquí</span></a> ó al número <span class="font-bold"> 018007028770.</span>
        </h2>
    </div>
</div>
