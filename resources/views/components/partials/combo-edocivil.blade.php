<x-input-4>
    <x-jet-label value="*Estado Civil"></x-jet-label>
    <select wire:model="id_estado_civil" class="form-control">
        <option value="">Estado Civil</option>
        @foreach ($estadoCivil as $civil)
            <option value="{{ $civil->id_estado_civil }}">{{ $civil->descripcion }}</option>
        @endforeach
    </select>
    @error('id_estado_civil') <span class="font-italic col-pink">{{ $message }}</span> @enderror
</x-input-4>
