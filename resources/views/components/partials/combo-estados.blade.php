<div>
    <x-input-4>
        <x-jet-label value="*Estado"></x-jet-label>
        <select wire:model="selectedEstado" class="form-control">
            <option value="">Estado</option>
            @foreach ($estados as $estado)
                <option value="{{ $estado->id_estado }}">{{ $estado->estado }}</option>
            @endforeach
        </select>
        @error('selectedEstado') <span class="font-italic col-pink">{{ $message }}</span> @enderror
    </x-input-4>

    @if (!is_null($municipios))

        <x-input-4>
            <x-jet-label value="*Municipio"></x-jet-label>
            <select wire:model="selectedMunicipio" class="form-control text-red ">
                <option value="">Municipio</option>
                @foreach ($municipios as $municipio)
                    <option value="{{ $municipio->id_municipio }}">{{ $municipio->municipio }}</option>
                @endforeach
            </select>
            <x-jet-input-error for="selectedMunicipio"></x-jet-input-error>
        </x-input-4>
    @endif

    @if (!is_null($colonias))

        <x-input-4>
            <x-jet-label value="*Colonias"></x-jet-label>
            <select wire:model="selectedColonia" class="form-control">
                <option value="">Colonia</option></option>
                @foreach ($colonias as $colonia)
                    <option value="{{ $colonia->id_colonia }}">{{ $colonia->colonia }}</option>
                @endforeach
            </select>
            <x-jet-input-error for="selectedColonia"></x-jet-input-error>
        </x-input-4>
    @endif
</div>
