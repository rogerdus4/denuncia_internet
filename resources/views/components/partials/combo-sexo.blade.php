<x-input-4>
    <x-jet-label value="*Sexo"></x-jet-label>
    <select wire:model="id_sexo" class="form-control">
        <option value="">Sexo</option>
        @foreach ($sexos as $sexo)
            <option value="{{ $sexo->id_sexo }}">{{ $sexo->sexo }}</option>
        @endforeach
    </select>
    @error('id_sexo') <span class="font-italic col-pink">{{ $message }}</span> @enderror
</x-input-4>
