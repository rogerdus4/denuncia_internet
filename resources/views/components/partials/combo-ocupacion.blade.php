<x-input-4>
    <x-jet-label value="*Ocupación"></x-jet-label>
    <select wire:model="id_ocupacion" class="form-control">
        <option value="">Ocupación</option>
        @foreach ($ocupaciones as $ocupacion)
            <option value="{{ $ocupacion->id_ocupacion }}">{{ $ocupacion->ocupacion }}</option>
        @endforeach
    </select>
    @error('id_ocupacion') <span class="font-italic col-pink">{{ $message }}</span> @enderror
</x-input-4>
