
    <x-input-4>
        <x-jet-label value="*Medio de Identificacion"></x-jet-label>
        <select wire:model="id_identificacion" class="form-control">
            <option value="">Medio de Identificacion</option>
            @foreach ($medioidentificacion as $identificacion)
                <option value="{{ $identificacion->id_identificacion }}">{{ $identificacion->identificacion }}</option>
            @endforeach
        </select>
        @error('id_identificacion') <span class="font-italic col-pink" >{{ $message }}</span> @enderror
    </x-input-4>
