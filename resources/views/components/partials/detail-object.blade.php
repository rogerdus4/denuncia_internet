<div>
    <div class="row">
        <div class="col-md-12">
            @foreach ($selObjetos as $index => $selObjeto)
                <div class="col-md-12">
                    <x-input-3>
                        <x-jet-label value="Objeto"></x-jet-label>
                        <select name="selObjetos[{{ $index }}][objeto_id]"
                            class="form-control"
                            wire:model="selObjetos.{{ $index }}.objeto_id">
                            <option value="">Seleccion un objeto</option>
                            @foreach ($tobjetos as $objeto)
                                <option value="{{ $objeto->id_tobjeto }}">
                                    {{ $objeto->tipo_objeto }}
                                </option>
                            @endforeach
                        </select>
                    </x-input-3>

                    <x-input-2>
                        <x-jet-label value="Compañia"></x-jet-label>
                        <input type="text" name="selObjetos[{{ $index }}][compania]"
                            class="form-control form-control"
                            wire:model="selObjetos.{{ $index }}.compania">
                        <small>Solo llenar en caso de ser teléfono</small>
                    </x-input-2>

                    <x-input-2>
                        <x-jet-label value="Marca"></x-jet-label>
                        <input type="text" name="selObjetos[{{ $index }}][marca]"
                            class="form-control form-control"
                            wire:model="selObjetos.{{ $index }}.marca">
                    </x-input-2>

                    <x-input-2>
                        <x-jet-label value="Modelo"></x-jet-label>
                        <input type="text" name="selObjetos[{{ $index }}][modelo]"
                            class="form-control form-control"
                            wire:model="selObjetos.{{ $index }}.modelo">
                    </x-input-2>
                    <x-input-2>
                        <x-jet-label value="Costo Aproximado"></x-jet-label>
                        <input type="number" name="selObjetos[{{ $index }}][costo]"
                            class="form-control form-control"
                            min="1" max="5000"
                            wire:model="selObjetos.{{ $index }}.costo">
                            <small>Artículos que cuyo valor intrínseco sea inferior a $5,000.00</small>
                    </x-input-2>
                    <x-input-1>
                        <i class="fas material-icons"
                            wire:click.prevent="removeObjeto({{ $index }})">delete</i>
                    </x-input-1>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12 text-center">
            <button class="btn btn-sm btn-secondary" wire:click.prevent="addObjeto">+ Agregar
                Objeto</button>
        </div>
    </div>
</div>
