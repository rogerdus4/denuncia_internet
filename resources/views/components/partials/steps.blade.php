<div>
    <div class="row">
        <div class="col-md-12 text-center">
            @if ($currentStep == 1)
                <div></div>
            @endif

            @if ($currentStep == 2 || $currentStep == 3 || $currentStep == 4)
                <x-jet-button class="btn  bg-grey waves-effect" style="font-size: 18px; border-radius:4px;"
                    wire:click="decreaseStep()">Regresar
                </x-jet-button>
            @endif

            @if ($currentStep == 1 || $currentStep == 2 || $currentStep == 3)
                <x-jet-button class="btn  bg-blue-grey waves-effect" style="font-size: 18px; border-radius:4px;"
                    wire:click="increaseStep()">Siguiente
                </x-jet-button>
            @endif

            @if ($currentStep == 4)
                <x-jet-button class="btn  bg-primary waves-effect" style="font-size: 18px; border-radius:4px;"
                wire:click="store">Enviar
                </x-jet-button>
            @endif
        </div>
    </div>
</div>
