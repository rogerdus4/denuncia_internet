<x-input-4>
    <x-jet-label value="*Punto de Referencia"></x-jet-label>
    <select wire:model="id_tlugar" class="form-control">
        <option value="">Selecciona</option>
        @foreach ($tipoLugares as $tlugar)
            <option value="{{ $tlugar->id_tlugar }}">{{ $tlugar->tipo_lugar }}</option>
        @endforeach
    </select>
    @error('id_tlugar') <span class="font-italic col-pink">{{ $message }}</span> @enderror
</x-input-4>
