<div>
    <div class="row">
        <div class="col-md-12">
            @foreach ($selDocumentos as $idx => $selDocumento)
                <x-input-3>
                    <x-jet-label value="Documento"></x-jet-label>
                    <select name="selDocumentos[{{ $idx }}][documento_id]"
                        class="form-control"
                        wire:model="selDocumentos.{{ $idx }}.documento_id">
                        <option value="">Seleccion un documento</option>
                        @foreach ($tdocumentos as $documento)
                            <option value=" {{ $documento->id_tdocumento }}">
                                {{ $documento->descripcion }}
                            </option>
                        @endforeach
                    </select>
                </x-input-3>
                <x-input-2>
                    <x-jet-label value="Especificaciones"></x-jet-label>
                    <input type="text"
                        name="selDocumentos[{{ $idx }}][especificaciones]"
                        class="form-control form-control"
                        wire:model="selDocumentos.{{ $idx }}.especificaciones">
                    <small>Solo llenar en caso de ser facturas,Resoluciones y otros
                        documentos</small>
                </x-input-2>
                <x-input-1>

                    <i class="fas material-icons"
                        wire:click.prevent="removeDocumento({{ $idx }})">delete</i>
                </x-input-1>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <button class="btn btn-sm btn-secondary" wire:click.prevent="addDocumento">+ Agregar
                Documento</button>
        </div>
    </div>
</div>
