<div>
    <x-input-4>
        <x-jet-label value="*Estado"></x-jet-label>
        <select wire:model="selectedEdoHechos" class="form-control">
            <option value="">Estado</option>
            @foreach ($edohechos as $edo)
                <option value="{{ $edo->id_estado }}">{{ $edo->estado }}</option>
            @endforeach
        </select>
        @error('selectedEdoHechos') <span class="font-italic col-pink">{{ $message }}</span> @enderror
    </x-input-4>

    @if (!is_null($munhechos))
        <x-input-4>
            <x-jet-label value="*Municipio"></x-jet-label>
            <select wire:model="selectedMunHechos" class="form-control">
                <option value="">Municipio</option>
                @foreach ($munhechos as $mun)
                    <option value="{{ $mun->id_municipio }}">{{ $mun->municipio }}</option>
                @endforeach
            </select>
            @error('selectedMunHechos') <span class="font-italic col-pink" >{{ $message }}</span> @enderror
        </x-input-4>
    @endif

    @if (!is_null($colhechos))
        <x-input-4>
            <x-jet-label value="*Colonia"></x-jet-label>
            <select wire:model="selectedColHechos" class="form-control">
                <option value="">Colonia</option>
                @foreach ($colhechos as $col)
                    <option value="{{ $col->id_colonia }}">{{ $col->colonia }}</option>
                @endforeach
            </select>
            @error('selectedColHechos') <span class="font-italic col-pink">{{ $message }}</span> @enderror
        </x-input-4>
    @endif


</div>
