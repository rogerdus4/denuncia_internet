<table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">Objeto</th>
        <th scope="col">Extravío</th>
        <th scope="col">Robo</th>
      </tr>
    </thead>
    <tbody>
        <tr>
            <td>    @if(isset($objeto)) {{  $objeto }} @endif</td>
            <td>    @if(isset($extravio))  {{ $extravio }}    @endif</td>
            <td>    @if(isset($robo)) {{  $robo }} @endif</td>
        </tr>
    </tbody>
</table>
