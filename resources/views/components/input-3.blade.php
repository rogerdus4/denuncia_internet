<div class="col-md-3">
    <div class="form-group">
        <div class="form-line">
            {{ $slot }}
        </div>
    </div>
</div>
