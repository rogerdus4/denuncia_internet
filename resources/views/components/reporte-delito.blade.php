<table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">Extravío</th>
        <th scope="col">Robo</th>
        <th scope="col">Total</th>
      </tr>
    </thead>
    <tbody>
        <tr>
            <td>    @if(isset($extravio)) {{  $extravio }} @endif</td>
            <td>    @if(isset($robo))  {{ $robo }}    @endif</td>
            <td>    @if(isset($extravio) && isset($robo)) {{ $extravio+$robo }} @endif </td>
        </tr>
    </tbody>
</table>
