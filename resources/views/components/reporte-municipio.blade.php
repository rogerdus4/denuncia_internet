<table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">Municipio</th>
        <th scope="col">Robo</th>
        <th scope="col">Extravío</th>
        <th scope="col">Total</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($r_municipio as $municipio)
            <tr>
                <td> @if(isset($municipio->municipio)) {{  $municipio->municipio }} @endif</td>
                <td> @if(isset($robo))  {{ $robo }}    @endif</td>
                <td> @if(isset($extravio)) {{ $extravio }} @endif </td>
                <td> @if(isset($robo) && isset($extravio)) {{ $robo+$extravio }} @endif </td>
            </tr>
        @endforeach
    </tbody>
</table>
