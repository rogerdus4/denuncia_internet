<div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        {{$slot}}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Multi Column -->
</div>
