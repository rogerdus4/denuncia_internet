<div class="col-md-6">
    <div class="form-group">
        <div class="form-line">
            {{ $slot }}
        </div>
    </div>
</div>
