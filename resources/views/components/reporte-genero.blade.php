<table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">Masculino</th>
        <th scope="col">Femenino</th>
        <th scope="col">Total</th>
      </tr>
    </thead>
    <tbody>
        <tr>
            <td>    @if(isset($masculino)) {{  $masculino }} @endif</td>
            <td>    @if(isset($femenino))  {{ $femenino }}    @endif</td>
            <td>    @if(isset($masculino) && isset($femenino)) {{ $femenino+$masculino }} @endif </td>
        </tr>
    </tbody>
</table>
