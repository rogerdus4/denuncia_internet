<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="card">
        <div class="header bg-cyan">
            <h2 class="text-center">
                DATOS DE LOS HECHOS<small></small>
            </h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <x-input-6>
                    <x-jet-label value="Fecha Hechos"></x-jet-label>
                    <x-jet-input type="text" class="w-full" value="{{ $hechos->fecha_hechos }}" :disabled="true">
                    </x-jet-input>
                </x-input-6>
                <x-input-6>
                    <x-jet-label value="Punto de Referencia"></x-jet-label>
                    <x-jet-input type="text" class="w-full" value="{{ $hechos->tipoLugar->tipo_lugar }}"
                        :disabled="true"></x-jet-input>
                </x-input-6>
            </div>
            <x-details.detail-localizacion :localizacion="$hechos->lugarHechos"></x-details.detail-localizacion>
            <div class="row clearfix">
                <x-input-12>
                    <x-jet-label value="Hechos"></x-jet-label>
                    <textarea class="form-control" style="height: 200px" minlength="150" maxlength="500" disabled="true"> {{ $hechos->hechos }}</textarea>
                </x-input-12>
            </div>
        </div>
    </div>
</div>
