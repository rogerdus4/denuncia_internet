<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="card">
        <div class="header bg-blue-grey">
            <h2 class="text-center">
                DATOS GENERALES <small></small>
            </h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <x-input-6>
                    <x-jet-label value="Denunciante"></x-jet-label>
                    <x-jet-input type="text"
                        value="{{ $persona->nom_persona }} {{ $persona->app_persona }} {{ $persona->apm_persona }}" :
                        :disabled="true"></x-jet-input>
                </x-input-6>
                <x-input-6>
                    <x-jet-label value="Fecha Nacimiento"></x-jet-label>
                    <x-jet-input type="text" value="{{ $persona->fecha_nacimiento }}" : :disabled="true">
                    </x-jet-input>
                </x-input-6>
            </div>
            <div class="row clearfix">
                <x-input-4>
                    <x-jet-label value="Sexo"></x-jet-label>
                    <x-jet-input type="text" value="{{ $persona->sexo->sexo }}" :disabled="true">
                    </x-jet-input>
                </x-input-4>
                <x-input-4>
                    <x-jet-label value="Estado Civil"></x-jet-label>
                    <x-jet-input type="text" value="{{ $persona->estadoCivil->descripcion }}" :disabled="true">
                    </x-jet-input>
                </x-input-4>
                <x-input-4>
                    <x-jet-label value="Telefono"></x-jet-label>
                    <x-jet-input type="text" value="{{ $persona->telefono }}" :disabled="true">
                    </x-jet-input>
                </x-input-4>
            </div>
            <div class="row clearfix">
                <x-input-4>
                    <x-jet-label value="Correo"></x-jet-label>
                    <x-jet-input type="text" value="{{ $persona->correo }}" :disabled="true">
                    </x-jet-input>
                </x-input-4>
                <x-input-4>
                    <x-jet-label value="RFC"></x-jet-label>
                    <x-jet-input type="text" value="{{ $persona->rfc }}" :disabled="true">
                    </x-jet-input>
                </x-input-4>
                <x-input-4>
                    <x-jet-label value="CURP"></x-jet-label>
                    <x-jet-input type="text" value="{{ $persona->curp }}" :disabled="true">
                    </x-jet-input>
                </x-input-4>
            </div>
            <x-details.detail-localizacion :localizacion="$persona->domicilio"></x-details.detail-localizacion>
        </div>
    </div>
    <div class="row clearfix ">
        <div class="col-lg-8 col-md-3 col-sm-6 col-xs-12 px-3.5">
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-cyan">
                    <i class="material-icons">save</i>
                </div>
                <div class="content">
                    <div class="text">Conserva tu numero de folio: {{ $denuncia->id_denuncia }}</div>
                    <div class="number">Descarga tu Constancia dando click <a href="{{ route('generaConstancia',['id' => $denuncia]) }}" target="_blank">aqui</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
