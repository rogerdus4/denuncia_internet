<div>
    <div class="row clearfix">
        <x-input-4>
            <x-jet-label value="Calle"></x-jet-label>
            <x-jet-input type="text" class="w-full" value="{{ $localizacion->calle }}" :disabled="true">
            </x-jet-input>
        </x-input-4>
        <x-input-4>
            <x-jet-label value="No. Ext."></x-jet-label>
            <x-jet-input type="text" class="w-full" value="{{ $localizacion->num_ext }}" :disabled="true">
            </x-jet-input>
        </x-input-4>
        <x-input-4>
            <x-jet-label value="No. Int"></x-jet-label>
            <x-jet-input type="text" class="w-full" value="{{ $localizacion->num_int }}" :disabled="true">
            </x-jet-input>
        </x-input-4>
    </div>
    <div class="row clearfix">
        <x-input-4>
            <x-jet-label value="Colonia"></x-jet-label>
            <x-jet-input type="text" class="w-full" value="{{ $localizacion->colonia->colonia }}" :disabled="true">
            </x-jet-input>
        </x-input-4>
        <x-input-4>
            <x-jet-label value="Municipio"></x-jet-label>
            <x-jet-input type="text" class="w-full" value="{{ $localizacion->municipio->municipio }}" :disabled="true"></x-jet-input>
        </x-input-4>
        <x-input-4>
            <x-jet-label value="Estado"></x-jet-label>
            <x-jet-input type="text" class="w-full" value="{{ $localizacion->estado->estado }}" :disabled="true"></x-jet-input>
        </x-input-4>
    </div>
    <div class="row clearfix">
        @if(isset($localizacion->referencia_1))
            <x-input-6>
                <x-jet-label value="Entre Calle: "></x-jet-label>
                <x-jet-input type="text" class="w-full" value="{{ $localizacion->referencia_1 }}" :disabled="true"></x-jet-input>
            </x-input-6>
        @endif
        @if(isset($localizacion->referencia_2))
            <x-input-6>
                <x-jet-label value="Y Calle: "></x-jet-label>
                <x-jet-input type="text" class="w-full" value="{{ $localizacion->referencia_2 }}" :disabled="true"></x-jet-input>
            </x-input-6>
        @endif
    </div>
</div>
