<table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">Documento</th>
        <th scope="col">Extravío</th>
        <th scope="col">Robo</th>
      </tr>
    </thead>
    <tbody>
        <tr>
            <td>    @if(isset($documento)) {{  $documento }} @endif</td>
            <td>    @if(isset($extravio))  {{ $extravio }}    @endif</td>
            <td>    @if(isset($robo))  {{ $robo }}    @endif</td>
        </tr>
    </tbody>
</table>
