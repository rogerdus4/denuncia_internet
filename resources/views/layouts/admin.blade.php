<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'FGJEM') }}</title>

    @livewireStyles

    <!-- Favicon-->
    <link rel="icon" href="{{ asset('images/logo_fgjem.png') }}" type="image/x-icon">
    <script src="{{ asset('jquery/jquery-3.3.1.min.js') }}"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
        type="text/css">

    <link href="https://fonts.googleapis.com/css2?family=Material+Icons" rel="stylesheet">
    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="{{ asset('plugins/light-gallery/css/lightgallery.css') }}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{ asset('css/style_admin.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('css/themes/all-themes.css') }}" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <!-- Dropzone Css-->
    <link href="{{ asset('dropzone/dropzone.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />

    <meta http-equiv="Content-Security-Policy-Report-Only: policy ">

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

</head>

<body class="theme-red">
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#/">
                   Panel de Administración
                </a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">Principal</li>
                    <li ng-class="url == '' ? 'active' : ''">
                        <a href="{{ route('admin') }}" class="waves-effect waves-block">
                            <i class="material-icons">home</i>
                            <span>Principal</span>
                        </a>
                    </li>
                    <li class="header">Constancias</li>
                    <li ng-class="url == 'download' ? 'active' : ''">
                        <a href="{{ route('admin') }}" class="waves-effect waves-block">
                            <i class="material-icons">description</i>
                            <span>Por folio</span>
                        </a>
                    </li>
                    <li ng-class="url == 'download' ? 'active' : ''">
                        <a href="{{ route('general') }}" class="waves-effect waves-block">
                            <i class="material-icons">list</i>
                            <span>General</span>
                        </a>
                    </li>
                    <li class="header">Reportes</li>
                    <li ng-class="url == 'file-structure' ? 'active' : ''">
                        <a href="{{ route('reportes') }}" class="waves-effect waves-block">
                            <i class="material-icons">newspaper</i>
                            <span>Reportes</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Current Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">

            <!-- Page Heading -->
            @if (isset($header))
                <header class="bg-white shadow">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
    {{-- @include('sweetalert::alert') --}}
    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/pages/forms/form-validation.js') }}"></script>

    <script src="{{ asset('js/pages/forms/advanced-form-elements.js') }}"></script>

    <!-- Dropzone Js-->
    <script src="{{ asset('dropzone/dropzone.js') }}"></script>

    @stack('modals')

    @livewireScripts

    @stack('scripts')
</body>
