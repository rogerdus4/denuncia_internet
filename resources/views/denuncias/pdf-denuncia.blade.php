<header>
    <style>
        body {
            margin: 0cm 0cm 0cm;
        }

        < !-- #encabezado {
            padding: 10px 0;
            width: 100%;
        }

        #encabezado2 {
            margin: auto;
            width: 100%;
            font-family: Arial, Calibri, sans-serif;
            font-size: 13px;
            font-weight: bold;
        }

        #encabezado .fila #col_1 {
            width: 25%
        }

        #encabezado .fila #col_2 {
            text-align: center;
            width: 50%;
        }

        #encabezado .fila #col_2 #span1 {
            font-family: Arial, Calibri, sans-serif;
            font-size: 18px;
            font-weight: bold;
        }

        #encabezado .fila #col_2 #span2 {
            font-size: 12px;
            color: #4d9;
        }

        #encabezado .fila #col_3 {
            width: 25%
        }

        #footer {
            padding-top: 5px 0;
            border-top: gray 1px solid;
            width: 100%;
        }

        #footer .fila td {
            text-align: right;
            width: 100%;
        }

        #footer .fila td span {
            font-size: 10px;
            color: grey;
        }

        body {
            font-family: Arial, Calibri, sans-serif;
        }

        /*.tr-1{background-color: #7b7878;font-weight: bold;}*/
        .tr-1 {
            background-color: #ccc;
            font-weight: bold;
        }

        .tr-2 {
            font-size: 10px;
        }

        /*
    .table td {width: 93%;border-collapse: collapse;font-family: Arial,Calibri;font-size: 10px;border:1px;}
    .table tr{border-top:0px;border-bottom: 0px;border-spacing: 0; }
    */
        .table1 {
            border: 0.3px solid #5D6D7E;
            padding: 8px;
            border-collapse: collapse;
            padding-top: 5px;
            margin-left: 30px;
            align: center;
            width: 80%;
        }

        .td1 {
            border: 0.3px solid #5D6D7E;
            padding: 1px;
            border-collapse: collapse;
            align: center;
        }

        .td2 {
            border: 0.3px solid #5D6D7E;
            padding: 1px;
            align: center;
        }


        .table {
            margin-left: 30px;
        }

        .table td {
            border-spacing: 0;
            width: 93%;
            border-collapse: collapse;
            font-family: Arial, Calibri, sans-serif;
            font-size: 10px;
            /*border-top:0px;*/
            border: 0.5px solid #5D6D7E;
            padding: 3px;
        }

        .tdTitle {

            height: 15px;
        }

        .tdTxt {
            border: 0.3px solid #5D6D7E;
            /*vertical-align: center;*/
        }

        .tdConcepto {
            background-color: #6E6E6E;
            color: white;
        }

        .tdConceptoTxt {
            border-bottom: 0px;
        }
    </style>
</header>

<table id="encabezado2" style="width:100%;padding-top: 20px;">
    <tr class="fila">
        <td style="width: 30%; text-align: left;">
            <img src="{{ public_path('images/edomex.png') }}" style="width: 30%;"">
        </td>
        <td style="width: 30%; text-align: right;">
            <img src="{{ public_path('images/logo-fgjem.png') }}" style="width: 20%;"">
        </td>

    </tr>
</table>

<table id="encabezado2" style="width:100%;">
    <tr>
        <td style="padding-bottom: 2px; text-align:center;"> 2023 Año de Francisco Villa, el revolucionario del pueblo</td>
    </tr>
</table>

<br>
<table class="table" style="width:98%;padding-bottom: 7px;" cellspacing="0">
    <col style="width:100%;">
    <tr>
        <td align="center" style="font-size: 12px;height: 18px; border:0; font-weight: bold;">CONSTANCIA DE RECEPCION DE
            DENUNCIAS</td>
    </tr>
    <tr>
        <td align="center" style="font-size: 12px;height: 18px; border:0; text-align: right; font-weight: bold; padding-right:40px;"">FOLIO:
            {{ $denuncia->id_denuncia }} </td>
    </tr>
    <tr>
        <td align="center" style="font-size: 12px;height: 17px; border:0; text-align: justify;"">Informo que su Denuncia
            de Hechos por el EXTRAVÍO de DOCUMENTOS ha sido registrada en los
            siguientes terminos:</td>
    </tr>
</table>

<table class="table1" cellspacing="0">
    <tr>
        <td class="td1" style="align:right; font-size: 12px; width:250px">Fecha y Hora de los Hechos</td>
        <td class="td1" style="align:left; font-size: 12px; width:382px">{{ $denuncia->hecho->fecha_hechos }} {{ $denuncia->hecho->hora_hechos }}</td>
    </tr>
</table>

<table class="table" style="width:98%;padding-bottom: 2px;" cellspacing="0">
    <tr>
        <td align="center" style="font-size: 12px; border:0; text-align: center;""> <span style="font-weight: bold;">
                DATOS DEL DENUNCIANTE</span>
    </tr>
</table>

<table class="table1" cellspacing="0">
    <tr>
        <td class="td1" style="aling:right; font-size: 12px;">Denunciante</td>
        <td colspan="3" class="td1" style="aling: left; font-size: 12px;">{{ $denuncia->persona->nom_persona }} {{  $denuncia->persona->app_persona  }} {{ $denuncia->persona->apm_persona }}</td>
    </tr>
    <tr>
        <td class="tdTxt" style="font-size: 12px;width:200px;">Ocupación:</td><td class="tdTxt" style="width:150px;font-size: 12px;"> {{ $denuncia->persona->ocupacion->ocupacion }}</td>
        <td class="tdTxt" style="font-size: 12px;aling:right;width:150px">Telefono: </td><td class="tdTxt" style="width:128px;font-size: 12px;"> {{ $denuncia->persona->telefono }}</td>
    </tr>
    <tr>
        <td class="td1" style="aling: right; font-size: 12px; ">Dirección: </td>
        <td colspan="3" class="td1" style="aling: left; font-size: 12px; ">{{ $denuncia->persona->domicilio->calle }} No. Ext: {{ $denuncia->persona->domicilio->num_ext }} No. Int: {{ $denuncia->persona->domicilio->num_int }}</td>
    </tr>
    <tr>
        <td class="td1" style="aling: right; font-size: 12px;">Colonia:</td>
        <td colspan="3" class="td1" style="aling: left; font-size: 12px;""> {{ $denuncia->persona->domicilio->colonia->colonia }} </td>
    </tr>
    <tr>
        <td class="td1" style="aling: right; font-size: 12px;">Municipio:</td>
        <td  colspan="3" class="td1" style="aling: left; font-size: 12px;">{{ $denuncia->persona->domicilio->municipio->municipio }} </td>
    </tr>
    <tr>
        <td class="tdTxt" style="font-size: 12px;">RFC:</td><td class="tdTxt" style="font-size: 12px;"> deded</td>
        <td class="tdTxt" style=" font-size: 12px;">Fecha Nacimiento: </td><td class="tdTxt" style=" font-size: 12px;"> {{ $denuncia->persona->fecha_nacimiento }}</td>
    </tr>
    <tr>
        <td class="tdTxt" style="font-size: 12px;">Sexo:</td><td class="tdTxt" style="font-size: 12px;"> {{ $denuncia->persona->sexo->sexo }}</td>
        <td class="tdTxt" style=" font-size: 12px;">Estado Civil: </td><td class="tdTxt" style=" font-size: 12px;"> {{ $denuncia->persona->estadoCivil->descripcion }}</td>
    </tr>
</table>

<table class="table" style="width:98%;padding-bottom: 2px;" cellspacing="0">
    <tr>
        <td align="center" style="font-size: 12px; border:0; text-align: center;""> <span
                style="font-weight: bold;">LUGAR DE LOS HECHOS</span>
    </tr>
</table>


<table class="table1" cellspacing="0">
    <tr>
        <td class="td1" style="aling:right; font-size: 12px;">Direccion</td>
        <td colspan="3" class="td1" style="aling: right; font-size: 12px;width:278px;"> {{ $denuncia->hecho->lugarHechos->calle }} No. Ext {{ $denuncia->hecho->lugarHechos->num_ext }} No. Int {{ $denuncia->hecho->lugarHechos->num_int }} </td>
    </tr>
    <tr>
        <td class="td1" style=" font-size:12px; align:right;">Colonia:</td>
        <td colspan="3" class="td1" style=" font-size:12px; align:left;"> {{ $denuncia->hecho->lugarHechos->colonia->colonia }} </td>
    </tr>
    <tr>
        <td class="td1" style=" font-size:12px; align:right;">Municipio: </td>
        <td colspan="3" class="td1" style=" font-size:12px; align:left;"> {{ $denuncia->hecho->lugarHechos->municipio->municipio }} </td>
    </tr>
    <tr>
        <td class="td1" style=" font-size:12px; align:right;">Punto de referencia:</td>
        <td colspan="3" class="td1" style=" font-size:12px; align:left;">{{ $denuncia->hecho->tipoLugar->tipo_lugar }} </td>
    </tr>
    <tr>
        <td class="td1" style=" font-size:12px; align:right;">Fecha y Hora: </td>
        <td colspan="3" class="td1" style=" font-size:12px; align:left;"> {{ $denuncia->hecho->fecha_hechos  }} {{ $denuncia->hecho->hora_hechos  }}</td>
    </tr>
    <tr>
        <td class="tdTxt" style="font-size: 12px;width:200px;">Entre calle :</td><td class="tdTxt" style="width:150px;font-size: 12px;"> {{ $denuncia->hecho->lugarHechos->referencia_1 }}</td>
        <td  class="tdTxt" style="font-size: 12px;aling:right;width:150px">Y Calle: </td><td class="tdTxt" style="width:128px;font-size: 12px;"> {{ $denuncia->hecho->lugarHechos->referencia_1 }}</td>
    </tr>
</table>

<table class="table" style="width:98%;padding-bottom: 7px;" cellspacing="0">
    <tr>
        <td style="font-size:12px;border:0; text-align: center;""> <span style="font-weight: bold;">NARRACION BREVE DE
                LOS HECHOS</span>
    </tr>
</table>

<table class="table" style="width:645px;font-size: 16px;height: 10%;padding-left: 6px;">
    <tr>
        <td style="font-size:12px;text-align:justify;">
           {{ $denuncia->hecho->hechos}}
        </td>
    </tr>
</table>


<table class="table" style="width:645px;padding-top:20px;">
    <tr>
        <td style="width:60%;font-size: 12px; border:0; text-align:justify;">Con fundamento en los artículos 223 del
            Código Nacional de Procedimientos Penales, 10 apartado A,
            fracción I, de la Ley Orgánica de la Procuraduría General de Justicia del Estado de México (Fiscalía General
            de Justicia del Estado de México), 6 del Reglamento de la Procuraduría General de Justicia del Estado de
            México (Fiscalía General de Justicia del Estado de México) y acuerdo 15/2011 del entonces Procurador
            General de Justicia del Estado de México, por el que se establecen los lineamientos para la presentación de
            denuncias de robo o extravío de documentos y objetos a través de medios electrónicos; se tiene por recibida
            la denuncia por el EXTRAVÍO de DOCUMENTOS la cual se registra con folio {{ $denuncia->id_denuncia }} en fecha {{ $denuncia->fecha_denuncia }}
            a las 11:57 horas. Así mismo se tiene por recibido los archivos adjuntos consistentes en CREDENCIAL DE
            ELECTOR.
            La presente constancia tiene validez oficial y surte efectos legales ante cualquier autoridad
            administrativa,
            laboral o jurisdiccional, únicamente sobre la denuncia realizada, sin prejuzgar sobre la veracidad de los
            hechos asentados. Lo anterior, sin perjuicio de que el ciudadano pueda presentarse a ratificar la denuncia o
            querella ante el Ministerio Público.
            Para verificar los datos de la constancia ingresa al portal de la Fiscalía General de Justicia del Estado de
            México o a la siguiente página http://denunciapgj.edomex.gob.mx/DenunciaInternet/Comprueba</td>
    </tr>
</table>
