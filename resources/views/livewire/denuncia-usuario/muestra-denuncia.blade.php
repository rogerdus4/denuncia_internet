<div>
    <x-details.detail-dgenerales :persona="$denuncia->persona" :denuncia="$denuncia"></x-details.detail-hechos>
    <x-details.detail-hechos :hechos="$denuncia->hecho"></x-details.detail-hechos>
</div>
