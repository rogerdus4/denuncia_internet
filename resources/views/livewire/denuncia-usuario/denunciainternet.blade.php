<div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <x-slot name="header" class="text-black">
            </x-slot>
            <div class="card">
                <x-partials.leyend></x-partials.leyend>
                @if ($currentStep == 1)
                    <div class="card-header bg-secondary text-white text-center">Paso 1 / 4 DATOS GENERALES</div>
                    <div class="header">
                        <small>Llena los datos correspondientes y oprime <span class="font-bold col-cyan">SIGUIENTE.</span></small>
                    </div>
                    <div class="body">
                        <div class="row clearfix">

                            <x-input-4>
                                <x-jet-label value="*Apellido Paterno"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="app_persona"></x-jet-input>
                                @error('app_persona')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="*Apellido Materno"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="apm_persona"></x-jet-input>
                                @error('apm_persona')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>
                            <x-input-4>
                                <x-jet-label value="*Nombre"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="nom_persona"></x-jet-input>
                                @error('nom_persona')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>
                        </div>
                        <div class="row clearfix">
                            <x-partials.combo-medioidentificacion :medioidentificacion="$medioidentificacion">
                            </x-partials.combo-medioidentificacion>
                            <x-input-4>
                                <x-jet-label value="*Num. De identificacion"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="no_identificacion">
                                </x-jet-input>
                                @error('no_identificacion')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>
                            <x-partials.combo-sexo :sexos="$sexos"></x-partials.combo-sexo>
                        </div>
                        <div class="row clearfix">
                            <x-partials.combo-edocivil :estadoCivil="$estado_civil"></x-partials.combo-edocivil>
                            <x-input-4>
                                <x-jet-label value="*Fecha de Nacimiento"></x-jet-label>
                                <x-jet-input type="date" class="w-full" wire:model.defer="fecha_nacimiento">
                                </x-jet-input>
                                @error('fecha_nacimiento')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>
                            <x-partials.combo-ocupacion :ocupaciones="$ocupaciones"></x-partials.combo-ocupacion>
                        </div>
                        <div class="row clearfix">
                            <x-input-3>
                                <x-jet-label value="RFC"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="rfc">
                                </x-jet-input>
                                @error('rfc')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>

                            <x-input-3>
                                <x-jet-label value="Curp"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="curp"></x-jet-input>
                                @error('curp')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>

                            <x-input-3>
                                <x-jet-label value="Teléfono"></x-jet-label>
                                <x-jet-input type="number" class="w-full" wire:model.defer="telefono"></x-jet-input>
                                @error('telefono')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>

                            <x-input-3>
                                <x-jet-label value="Correo Electronico"></x-jet-label>
                                <x-jet-input type="email" class="w-full" wire:model.defer="correo"></x-jet-input>
                                @error('correo')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>
                        </div>
                    </div>
                @endif

                @if ($currentStep == 2)
                    <div class="card-header bg-secondary text-white text-center">Paso 2 / 4 DOMICILIO</div>
                    <div class="header">
                        <small>LLena la informacion Correspondiente</small>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <x-partials.combo-estados :estados="$estados" :municipios="$municipios" :colonias="$colonias">
                            </x-partials.combo-estados>
                        </div>
                        <div class="row clearfix">
                            <x-input-3>
                                <x-jet-label value="*Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="calle">
                                </x-jet-input>
                                @error('calle')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>

                            <x-input-3>
                                <x-jet-label value="*Número Exterior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="ext"></x-jet-input>
                                @error('ext')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>

                            <x-input-3>
                                <x-jet-label value="Número Interior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="int"></x-jet-input>
                                @error('int')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>
                            <x-input-3>
                                <x-jet-label value="*Codigo Postal"></x-jet-label>
                                <x-jet-input type="number" class="w-full" wire:model.defer="cp"></x-jet-input>
                                @error('cp')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-3>
                        </div>
                    </div>
                @endif
                @if ($currentStep == 3)
                    <div class="card-header bg-secondary text-white text-center">Paso 3 / 4 LUGAR DE LOS HECHOS</div>
                    <div class="header">
                        <small>LLena la informacion Correspondiente</small>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <x-input-6>
                                <x-jet-label value="*Fecha"></x-jet-label>
                                <x-jet-input type="date" class="w-full" wire:model.defer="fecha_hechos">
                                </x-jet-input>
                                @error('fecha_hechos')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-6>

                            <x-input-6>
                                <x-jet-label value="*Hora Hechos"></x-jet-label>
                                <x-jet-input type="time" class="w-full" wire:model.defer="hora_hechos">
                                </x-jet-input>
                                @error('hora_hechos')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-6>
                        </div>
                        <div class="row clearfix">
                            <x-partials.combo-hechos :edohechos="$edohechos" :munhechos="$munhechos" :colhechos="$colhechos">
                            </x-partials.combo-hechos>
                        </div>
                        <div class="row clearfix">
                            <x-input-4>
                                <x-jet-label value="Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="calle_hechos">
                                </x-jet-input>
                                @error('calle_hechos')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Número Exterior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="ext_hechos"></x-jet-input>
                                @error('ext_hechos')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Número Interior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="int_hechos"></x-jet-input>
                                @error('int_hechos')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>
                        </div>
                        <div class="row clearfix">
                            <x-partials.combo-tlugar :tipoLugares="$tipoLugares"></x-partials.combo-tlugar>
                            <x-input-4>
                                <x-jet-label value="Entre Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="referencia_1">
                                </x-jet-input>
                                @error('referencia_1')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value=" Y Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model.defer="referencia_2">
                                </x-jet-input>
                                @error('referencia_2')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                            </x-input-4>
                        </div>
                        <div class="row clearfix">
                            <x-input-12>
                                <x-jet-label value="*Narracion de los Hechos"></x-jet-label>
                                <textarea class="form-control" wire:model.defer="hechos" style="height: 200px" minlength="150" maxlength="500" required></textarea>
                                @error('hechos')
                                    <span class="font-italic col-pink" >{{ $message }}</span>
                                @enderror
                                <small>Máximo 500 caracteres</small>
                            </x-input-12>
                        </div>
                    </div>
                @endif

                @if ($currentStep == 4)
                    <div class="card-header bg-secondary text-white text-center">Paso 4 / 4 Documentos y Objetos</div>
                    <div class="header">
                        <small>LLena la informacion Correspondiente</small>
                    </div>
                    <div class="body">
                        <ol class="breadcrumb breadcrumb-bg-pink">
                            <li>DOCUMENTOS</li>
                        </ol>
                        <x-partials.detail-document :selDocumentos="$selDocumentos" :tdocumentos="$tdocumentos"></x-partials.detail-document>
                        <div class="col-md-12">
                            <div class="alert bg-red">
                                ** Este trámite no tendrá validez tratándose de robo de Placas de Circulación y Tarjeta
                                de Circulación, para lo cual, deberá acudir en forma personal a las Agencias del
                                Ministerio Público más cercanas.
                            </div>
                        </div>
                        <ol class="breadcrumb breadcrumb-bg-pink">
                            <li>OBJETOS</li>
                        </ol>
                        <x-partials.detail-object :selObjetos="$selObjetos" :tobjetos="$tobjetos"></x-partials.detail.object>
                            <div class="col-md-12">
                                <div class="alert bg-red">
                                    **Como medio de seguridad no se permitirá describir tarjetas de débito o crédito así como vehículos
                                </div>
                            </div>
                    </div>
                @endif

                <div class="demo-button-groups p-4">
                    <x-partials.steps :currentStep="$currentStep"></x-partials.steps>
                </div>
            </div>
        </div>
    </div>
</div>
