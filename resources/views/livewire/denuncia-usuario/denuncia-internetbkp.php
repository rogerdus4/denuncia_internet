<div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <x-slot name="header" class="text-black">
            </x-slot>
            <div class="card">
                <div class="body">
                    <div class="body">
                        <ol class="breadcrumb breadcrumb-bg-pink">
                            <li>Datos Generales</li>
                        </ol>
                    </div>

                    <form method="POST" action="">
                        <div class="row clearfix">
                            <x-input-4>
                                <x-jet-label  value="*Apellido Paterno"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="apaterno"></x-jet-input>
                                @error('apaterno') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="*Apellido Materno"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="amaterno"></x-jet-input>
                                @error('amaterno') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="*Nombre"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="nombre"></x-jet-input>
                                @error('nombre') <span>{{ $message }}</span> @enderror
                            </x-input-4>
                        </div>

                        <div class="row clearfix">

                            @include(
                                'livewire.denuncia-usuario.combo-medioiden'
                            )

                            <x-input-4>
                                <x-jet-label value="*Num. De identificacion"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="no_identificacion">
                                </x-jet-input>
                                @error('no_identificacion') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            @include('livewire.denuncia-usuario.combo-sexo')

                        </div>

                        <div class="row clearfix">

                            @include(
                                'livewire.denuncia-usuario.combo-edocivil'
                            )

                            <x-input-4>
                                <x-jet-label value="*Fecha de Nacimiento"></x-jet-label>
                                <x-jet-input type="date" class="w-full" wire:model="f_nacimiento">
                                </x-jet-input>
                                @error('f_nacimiento') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            @include(
                                'livewire.denuncia-usuario.combo-ocupacion'
                            )
                        </div>

                        <div class="row clearfix">
                            <x-input-4>
                                <x-jet-label value="RFC"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="rfc">
                                </x-jet-input>
                                @error('rfc') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Curp"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="curp"></x-jet-input>
                                @error('curp') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Correo Electronico"></x-jet-label>
                                <x-jet-input type="email" class="w-full" wire:model="correo"></x-jet-input>
                                @error('correo') <span>{{ $message }}</span> @enderror
                            </x-input-4>
                        </div>

                        <div class="row clearfix">
                            <x-input-12>
                                <x-jet-label value="*Adjuntar archivo con el que se identifica"></x-jet-label>
                                <input type="file" class="w-full" wire:model="medio_adjunto">
                                @error('medio_adjunto') <span>{{ $message }}</span> @enderror
                                <small>Sólo (jpg, jpeg,png,gif), máximo: 250Kb</small>
                                </x-input-4>
                        </div>

                        <div class="body">
                            <ol class="breadcrumb breadcrumb-bg-pink">
                                <li>Domicilio</li>
                            </ol>
                        </div>

                        <div class="row clearfix">

                            @include(
                                'livewire.denuncia-usuario.combo-estados'
                            )

                        </div>

                        <div class="row clearfix">
                            <x-input-4>
                                <x-jet-label value="*Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="calle">
                                </x-jet-input>
                                @error('calle') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="*Número Exterior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="ext"></x-jet-input>
                                @error('ext') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Número Interior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="int"></x-jet-input>
                                @error('int') <span>{{ $message }}</span> @enderror
                            </x-input-4>
                        </div>

                        <div class="row clearfix">
                            <x-input-6>
                                <x-jet-label value="*Codigo Postal"></x-jet-label>
                                <x-jet-input type="number" class="w-full" wire:model="cp"></x-jet-input>
                                @error('cp') <span>{{ $message }}</span> @enderror
                            </x-input-6>

                            <x-input-6>
                                <x-jet-label value="Teléfono"></x-jet-label>
                                <x-jet-input type="number" class="w-full" wire:model="telefono"></x-jet-input>
                                @error('telefono') <span>{{ $message }}</span> @enderror
                            </x-input-6>
                        </div>

                        <div class="body">
                            <ol class="breadcrumb breadcrumb-bg-pink">
                                <li>Lugar de los Hechos</li>
                            </ol>
                        </div>

                        <div class="row clearfix">
                            <x-input-6>
                                <x-jet-label value="*Fecha"></x-jet-label>
                                <x-jet-input type="date" class="w-full" wire:model="fecha_hechos">
                                </x-jet-input>
                                @error('fecha_hechos') <span>{{ $message }}</span> @enderror
                            </x-input-6>

                            <x-input-6>
                                <x-jet-label value="*Hora Hechos"></x-jet-label>
                                <x-jet-input type="time" class="w-full" wire:model="hora_hechos">
                                </x-jet-input>
                                @error('hora_hechos') <span>{{ $message }}</span> @enderror
                            </x-input-6>
                        </div>

                        <div class="row clearfix">
                            @include('livewire.denuncia-usuario.combo-hechos')
                        </div>

                        <div class="row clearfix">
                            <x-input-4>
                                <x-jet-label value="Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="calle_hechos">
                                </x-jet-input>
                                @error('calle_hechos') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Número Exterior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="ext_hechos"></x-jet-input>
                                @error('ext_hechos') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Número Interior"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="int_hechos"></x-jet-input>
                                @error('int_hechos') <span>{{ $message }}</span> @enderror
                            </x-input-4>
                        </div>

                        <div class="row clearfix">
                            <x-input-4>
                                <x-jet-label value="*Punto de Referencia"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="lugar_hechos">
                                </x-jet-input>
                                @error('lugar_hechos') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value="Entre Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="referencia_1">
                                </x-jet-input>
                                @error('referencia_1') <span>{{ $message }}</span> @enderror
                            </x-input-4>

                            <x-input-4>
                                <x-jet-label value=" Y Calle"></x-jet-label>
                                <x-jet-input type="text" class="w-full" wire:model="referencia_2">
                                </x-jet-input>
                                @error('referencia_2') <span>{{ $message }}</span> @enderror
                            </x-input-4>
                        </div>

                        <x-input-12>
                            <x-jet-label value="*Narracion de los Hechos"></x-jet-label>
                            <textarea class="form-control" wire:model="hechos" style="height: 200px" minlength="150"
                                maxlength="500" required></textarea>
                                @error('hechos') <span>{{ $message }}</span> @enderror
                            <small>Máximo 500 caracteres</small>
                        </x-input-12>

                        <div class="body">
                            <ol class="breadcrumb breadcrumb-bg-pink">
                                <li>Documentos</li>
                            </ol>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @foreach ($selDocumentos as $idx => $selDocumento)
                                    {{-- <div class="col-md-12"> --}}
                                        <x-input-3>
                                            <x-jet-label value="Documento"></x-jet-label>
                                            <select name="selDocumentos[{{$idx}}][documento_id]"
                                                class="form-control"
                                                wire:model="selDocumentos.{{$idx}}.documento_id">
                                                <option value="">Seleccion un documento</option>
                                                @foreach ($tdocumentos as $documento)
                                                    <option value=" {{ $documento->id_tdocumento}}">
                                                        {{ $documento->descripcion }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </x-input-3>
                                        <x-input-2>
                                            <x-jet-label value="Especificaciones"></x-jet-label>
                                            <input type="text" name="selDocumentos[{{ $idx }}][especificaciones]"
                                                class="form-control form-control"
                                                wire:model="selDocumentos.{{$idx}}.especificaciones">
                                             <small>Solo llenar en caso de ser facturas,Resoluciones y otros documentos</small>
                                        </x-input-2>
                                    {{-- </div> --}}
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-sm btn-secondary"
                                    wire:click.prevent="addDocumento">+ Agregar Documento</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="alert bg-red">
                               ** Este trámite no tendrá validez tratándose de robo de Placas de Circulación y Tarjeta de Circulación, para lo cual, deberá acudir en forma personal a las Agencias del Ministerio Público más cercanas.
                            </div>
                        </div>
                        <div class="body">
                            <ol class="breadcrumb breadcrumb-bg-pink">
                                <li>Objetos</li>
                            </ol>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @foreach ($selObjetos as $index => $selObjeto)
                                <div class="col-md-12">
                                    <x-input-3>
                                        <x-jet-label value="Objeto"></x-jet-label>
                                        <select name="selObjetos[{{ $index }}][objeto_id]"
                                            class="form-control"
                                            wire:model="selObjetos.{{$index}}.objeto_id">
                                            <option value="">Seleccion un objeto</option>
                                            @foreach ($tobjetos as $objeto)
                                                <option value="{{ $objeto->id_tobjeto }}">
                                                    {{ $objeto->tipo_objeto }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </x-input-3>

                                    <x-input-2>
                                        <x-jet-label value="Compañia"></x-jet-label>
                                        <input type="text" name="selObjetos[{{ $index }}][compania]"
                                            class="form-control form-control"
                                            wire:model="selObjetos.{{$index}}.compania">
                                            <small>Solo llenar en caso de ser teléfono</small>
                                    </x-input-2>

                                    <x-input-2>
                                        <x-jet-label value="Marca"></x-jet-label>
                                        <input type="text" name="selObjetos[{{ $index }}][marca]"
                                            class="form-control form-control"
                                            wire:model="selObjetos.{{$index}}.marca">
                                    </x-input-2>

                                    <x-input-2>
                                        <x-jet-label value="Modelo"></x-jet-label>
                                        <input type="text" name="selObjetos[{{ $index }}][modelo]"
                                            class="form-control form-control"
                                            wire:model="selObjetos.{{$index}}.modelo">
                                    </x-input-2>
                                    <x-input-2>
                                        <x-jet-label value="Costo Aproximado"></x-jet-label>
                                        <input type="text" name="selObjetos[{{ $index }}][costo]"
                                            class="form-control form-control"
                                            wire:model="selObjetos.{{$index}}.costo">
                                    </x-input-2>
                                    <x-input-1>
                                        <i class="fas material-icons" wire:click.prevent="removeObjeto({{$index}})">delete</i>
                                    </x-input-1>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-sm btn-secondary"
                                    wire:click.prevent="addObjeto">+ Agregar Objeto</button>
                            </div>
                        </div>
                        <div class="footer">
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                        <x-jet-danger-button class="btn btn-primary btn-lg waves-effect"
                                        style="font-size: 18px; border-radius:5px;"
                                        wire:click="store" wire:loading.attr="disabled">
                                            Enviar
                                        </x-jet-danger-button>
                                </div>
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- #END# Multi Column -->

    </div>
