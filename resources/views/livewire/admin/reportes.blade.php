<div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <x-slot name="header" class="text-black">

            </x-slot>
            <div class="card">
                <div class="header">
                    Reportes
                </div>
                <div class="body">
                    <form wire:submit.prevent="submit" method="POST">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <div class="form-check">
                                        <input class="form-check-input with-gap" type="radio" name="filtros" id="filtros1" wire:model.defer="filtros" checked="false" value="delito">
                                        <label class="form-check-label" for="filtros1">
                                            Delito
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input with-gap" type="radio" name="filtros" id="filtros2" wire:model.defer="filtros" checked="false" value="genero">
                                        <label class="form-check-label" for="filtros2">
                                            Género
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input with-gap" type="radio" name="filtros" id="filtros3" wire:model.defer="filtros" checked="false" value="municipio">
                                        <label class="form-check-label" for="filtros3">
                                            Municipio
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input with-gap" type="radio" name="filtros" id="filtros4" wire:model.defer="filtros" checked="false" value="documentos">
                                        <label class="form-check-label" for="filtros4">
                                            Documentos
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input with-gap" type="radio" name="filtros" id="filtros5" wire:model.defer="filtros" checked="false" value="objetos">
                                        <label class="form-check-label" for="filtros5">
                                            Objetos
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input with-gap" type="radio" name="filtros" id="filtros6"  wire:model.defer="filtros" checked="false" value="general">
                                        <label class="form-check-label" for="filtros6">
                                            General
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <input type="date" name="f_inicial" id="f_inicial" wire:model.defer="f_inicial" class="form-control">
                                    @error('f_inicial') <span>{{ $message }}</span> @enderror
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <input type="date" name="f_final" id="f_final" wire:model.defer="f_final" class="form-control">
                                    @error('f_final') <span>{{ $message }}</span> @enderror
                                </div>
                                <br>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary waves-effect">Consultar</button>
                                </div>
                            </div>
                        </form>

                        <input type="text" value="{{$filtros}}">
                        @if($filtros=="delito")
                            @foreach ($r_delito as $delito)
                                @if($delito->tipo_denuncia==1)
                                    @php
                                        $robo++;
                                    @endphp
                                @else
                                    @php
                                        $extravio++;
                                    @endphp
                                @endif
                            @endforeach

                            <x-tabla-folio>
                                <x-reporte-delito></x-reporte-delito>
                            </x-tabla-folio>
                        @endif

                        @if($filtros=="genero")
                            @foreach ($r_genero as $genero)
                                @if($genero->sexo==1)
                                    @php
                                        $masculino++;
                                    @endphp
                                @else
                                    @php
                                        $femenino++;
                                    @endphp
                                @endif
                            @endforeach

                            <x-tabla-folio>
                                <x-reporte-genero></x-reporte-genero>
                            </x-tabla-folio>
                        @endif

                        @if($filtros=="municipio")
                        {{$r_documento}}
                        @php dd($r_documento); @endphp
                        <!-- No llega la variable  r_reporte a la vista -->
                            <x-tabla-folio>
                                <x-reporte-municipio></x-reporte-municipio>
                            </x-tabla-folio>
                        @endif

                        @if($filtros=="documentos")
                            @foreach ($r_documento as $genero)
                                @if($genero->sexo==1)
                                    @php
                                        $masculino++;
                                    @endphp
                                @else
                                    @php
                                        $femenino++;
                                    @endphp
                                @endif
                            @endforeach

                            <x-tabla-folio>
                                <x-reporte-documentos></x-reporte-genero>
                            </x-tabla-folio>
                        @endif

                        @if($filtros=="objetos")
                            @foreach ($r_objeto as $objeto)
                                @if($objeto->sexo==1)
                                    @php
                                        $extravio++;
                                    @endphp
                                @else
                                    @php
                                        $robo++;
                                    @endphp
                                @endif
                            @endforeach

                            <x-tabla-folio>
                                <x-reporte-objetos></x-reporte-objetos>
                            </x-tabla-folio>
                        @endif


                        @if($filtros=="general")
                            @foreach ($r_objeto as $objeto)
                                @if($objeto->sexo==1)
                                    @php
                                        $extravio++;
                                    @endphp
                                @else
                                    @php
                                        $robo++;
                                    @endphp
                                @endif
                            @endforeach

                            <x-tabla-folio>
                                <x-reporte-general></x-reporte-objetos>
                            </x-tabla-folio>
                        @endif


                </div>
            </div>


            {{-- @livewire('admin.consulta-folio-individual') --}}

        </div>
        <!-- #END# Multi Column -->
    </div>
