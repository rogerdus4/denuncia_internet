<div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <x-slot name="header" class="text-black">

            </x-slot>
            <div class="card">
                <div class="header">
                    ADMIN
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <form>
                            <label for="c_folio">Consulta de folios</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="c_folio" name="c_folio" class="form-control"  wire:model="c_folio" placeholder="Ingresa el número de folio">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                </div>
                            </div>

                            <br>
                            {{-- <button type="submit" class="btn btn-primary m-t-15 waves-effect">Consultar</button> --}}
                        </form>
                    </div>

                    <x-tabla-folio>
                        <table class="table">
                            <thead class="table-dark">
                              <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellido Paterno</th>
                                <th scope="col">Apellido Materno</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Correo</th>
                                <th scope="col">No. Identificación</th>
                                <th scope="col">Edad</th>
                                <th scope="col">Fecha Nacimiento</th>
                                <th scope="col">RFC</th>
                                <th scope="col">CURP</th>
                                <th scope="col">Fecha Hechos</th>
                                <th scope="col">Hora Hechos</th>
                                <th scope="col">Descripción Objeto</th>
                                <th scope="col">Especificaciones documento</th>
                                <th scope="col">Hechos</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($denuncias as $denuncia)
                                    <tr>
                                        <td>{{ $denuncia->id_denuncia }}</td>
                                        <td>{{ $denuncia->nom_persona }}</td>
                                        <td>{{ $denuncia->app_persona }}</td>
                                        <td>{{ $denuncia->apm_persona }}</td>
                                        <td>{{ $denuncia->telefono }}</td>
                                        <td>{{ $denuncia->correo }}</td>
                                        <td>{{ $denuncia->no_identificacion }}</td>
                                        <td>{{ $denuncia->edad }}</td>
                                        <td>{{ $denuncia->fecha_nacimiento }}</td>
                                        <td>{{ $denuncia->rfc }}</td>
                                        <td>{{ $denuncia->curp }}</td>
                                        <td>{{ $denuncia->fecha_hechos }}</td>
                                        <td>{{ $denuncia->hora_hechos }}</td>
                                        <td>{{ $denuncia->descripcion }}</td>
                                        <td>{{ $denuncia->especificaciones }}</td>
                                        <td>{{ $denuncia->hechos }}</td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- {{ $denuncias->links('pagination::Bootstrap-4') }} --}}
                    </x-tabla-folio>

                </div>
            </div>
            {{-- @livewire('admin.consulta-folio-individual') --}}

        </div>
        <!-- #END# Multi Column -->
    </div>
