<?php

use App\Http\Controllers\DenunciasController;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\DenunciaUsuario\Denunciainternet;
use App\Http\Livewire\Admin\Principal;
use App\Http\Livewire\Admin\General;
use App\Http\Livewire\Admin\Reportes;
use App\Http\Livewire\DenunciaUsuario\MuestraDenuncia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/denunciainternet', Denunciainternet::class)->name('denunciainternet');
Route::get('/muestraConstancia/{id}',MuestraDenuncia::class)->name('muestraConstancia');
Route::get('/generaConstancia/{id}',[DenunciasController::class,'generaPdf'])->name('generaConstancia');


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//Rutas de administador
Route::get('/admin', Principal::class)->name('admin');
Route::get('/general', General::class)->name('general');
Route::get('/reportes', Reportes::class)->name('reportes');



