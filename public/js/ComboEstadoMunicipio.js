﻿$(document).ready(function() {

    var id_estado = localStorage.getItem('id_estado');
    console.log(id_estado);
    if (id_estado != null) {
        var cURL = window.location.hostname;
        if (cURL == '127.0.0.1') {
            cURL = 'http://127.0.0.1:8000';
        }
        if (cURL == 'fgjem.tech') {
            cURL = 'https://fgjem.tech/sis/predenuncia';
        }
        url = cURL + "/combo_municipios/";

        $.ajax({
            url: url,
            type: 'post',
            data: {
                "id_estado": id_estado,
                "_token": $("meta[name='csrf-token']").attr("content")
            },
            success: function(data) {
                $("#id_municipio").html(LLenadoComboMunicipio(data));
                cambio = cambiar_municipio();
                if (cambio != "" && cambio != null) {
                    console.log("cambio");
                    console.log(cambio);
                    cambio_colonia_combo();
                }
            }
        });
    }

});

//combo de modelo vehiculo
$("#id_estado").change(function() {
    var id_estado = $("#id_estado").val();
    localStorage.setItem('id_estado', id_estado);

    var cURL = window.location.hostname;
    if (cURL == '127.0.0.1') {
        cURL = 'http://127.0.0.1:8000';
    }
    if (cURL == 'fgjem.tech') {
        cURL = 'https://fgjem.tech/sis/predenuncia';
    }
    url = cURL + "/combo_municipios/";

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_estado": id_estado,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            $("#id_municipio").html(LLenadoComboMunicipio(data));
        }
    });


});

$("#id_municipio").change(function() {
    var a = this.options[this.selectedIndex].value;
    localStorage.setItem('id_municipio', a);

    var url_rout = window.location.hostname;
    if (url_rout == '127.0.0.1') {
        url_rout = 'http://127.0.0.1:8000';
    }
    if (url_rout == 'fgjem.tech') {
        url_rout = 'https://fgjem.tech/sis/predenuncia';
    }
    url_rout = url_rout + "/combo_coloniasGet/";

    $("#colonia_den").load(url_rout + '?id_municipios=' + a);
});

$("#colonia_den").change(function() {
    var id_colonia = this.options[this.selectedIndex].value;
    localStorage.setItem('colonia_den', id_colonia);
});

function LLenadoComboMunicipio(data) {
    var sm;
    sm += '<option value="">Selecciona</option>';
    for (var i in data) {
        sm += "<option value='" + data[i].id_municipios + "'>" + data[i].municipio + "</option>";
    }
    return sm;
}

function cambiar_municipio() {
    var id_municipio = localStorage.getItem('id_municipio');
    $('#id_municipio').val(id_municipio).change();

    return id_municipio;
}

function cambio_colonia_combo() {
    console.log("entro a la funcion");
    var id_municipio = localStorage.getItem('id_municipio');

    if (id_municipio != null) {

        var url_rout = window.location.hostname;
        if (url_rout == '127.0.0.1') {
            url_rout = 'http://127.0.0.1:8000';
        }
        if (url_rout == 'fgjem.tech') {
            url_rout = 'https://fgjem.tech/sis/predenuncia';
        }
        url_rout = url_rout + "/combo_coloniasGet/";

        $("#colonia_den").load(url_rout + '?id_municipios=' + id_municipio,
            function() {
                var colonia_den = localStorage.getItem('colonia_den');
                console.log(colonia_den);
                $('#colonia_den').val(colonia_den).change();
            });
    }
}

$(document).ready(function() {
    var id_municipio1 = localStorage.getItem('id_municipio1');
    if (id_municipio1 !== null) {

        var url_rout = window.location.hostname;
        if (url_rout == '127.0.0.1') {
            url_rout = 'http://127.0.0.1:8000';
        }
        if (url_rout == 'fgjem.tech') {
            url_rout = 'https://fgjem.tech/sis/predenuncia';
        }
        url_rout = url_rout + "/combo_coloniasGet/";

        $("#colonia_hechos").load(url_rout + '?id_municipios=' + id_municipio1,
            function() {
                var colonia_hechos = localStorage.getItem('colonia_hechos');
                $('#colonia_hechos').val(colonia_hechos).change();
            });
    }
});

$("#id_municipio1").change(function() {
    var a = this.options[this.selectedIndex].value;
    localStorage.setItem('id_municipio1', a);

    var url_rout = window.location.hostname;
    if (url_rout == '127.0.0.1') {
        url_rout = 'http://127.0.0.1:8000';
    }
    if (url_rout == 'fgjem.tech') {
        url_rout = 'https://fgjem.tech/sis/predenuncia';
    }
    url_rout = url_rout + "/combo_coloniasGet/";

    $("#colonia_hechos").load(url_rout + '?id_municipios=' + a);

    $("#colonia_hechos").change(function() {
        var colonia_hechos = this.options[this.selectedIndex].value;
        localStorage.setItem('colonia_hechos', colonia_hechos);
    });
});

$("#colonia_hechos").change(function() {
    var a = this.options[this.selectedIndex].value;
    localStorage.setItem('colonia_hechos', a);
});