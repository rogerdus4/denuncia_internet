﻿$(document).ready(function() {

    var id_marca = localStorage.getItem('marca_vehiculo');
    if (id_marca != null) {
        var cURL = window.location.hostname;
        console.log(cURL);
        if (cURL == '127.0.0.1') {
            cURL = 'http://127.0.0.1:8000';
        }
        if (cURL == 'fgjem.tech') {
            cURL = 'https://fgjem.tech/sis/predenuncia';
        }
        url = cURL + "/combo_submarca/";

        $.ajax({
            url: url,
            type: 'post',
            data: {
                "id_marca": id_marca,
                "_token": $("meta[name='csrf-token']").attr("content")
            },
            success: function(data) {
                // $("#modelo_vehiculo_denunciante").val(data);
                $("#modelo_vehiculo_denunciante").html(LLenadoComboV(data));
                var id_modelo = localStorage.getItem('modelo_vehiculo');
                $('#modelo_vehiculo_denunciante').val(id_modelo).change();
            }
        });
    }


});

$("#tipo_placa").change(function() {
    var a = this.options[this.selectedIndex].value;

    if (a == 2) {
        //alert("Atención", "La placa es de tipo federal la cual no compete a la FGJEM", "warning");
        swal('Atención', 'La placa es de tipo federal la cual no compete a la FGJEM', 'warning');
    }
});



//combo de marca vehiculo
$("#marca_vehiculo_denunciante").change(function() {

    var id_marca = $("#marca_vehiculo_denunciante").val();
    localStorage.setItem('marca_vehiculo', id_marca);

    var cURL = window.location.hostname;
    //console.log(id_marca);
    console.log(cURL);
    if (cURL == '127.0.0.1') {
        cURL = 'http://127.0.0.1:8000';
    }
    if (cURL == 'fgjem.tech') {
        cURL = 'https://fgjem.tech/sis/predenuncia';
    }
    url = cURL + "/combo_submarca/";

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_marca": id_marca,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            // $("#modelo_vehiculo_denunciante").val(data);
            $("#modelo_vehiculo_denunciante").html(LLenadoComboV(data));
        }
    });
});

//combo de modelo vehiculo
$("#modelo_vehiculo_denunciante").change(function() {
    var id_modelo = $("#modelo_vehiculo_denunciante").val();
    localStorage.setItem('modelo_vehiculo', id_modelo);
});

function LLenadoComboV(data) {
    var sm;
    sm += '<option value="" id="modelo_vehiculo_denunciante" style="width:200px">Seleccione</option>';
    for (var i in data) {
        sm += "<option value='" + data[i].id_submarca + "' id='modelo_vehiculo_denunciante' style='width:200px'>" + data[i].submarca + "</option>";
    }
    return sm;
}