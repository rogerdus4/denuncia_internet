//// combo de codigo postal
$(".id_colonia").change(function() {

    var id_colonia = $(".id_colonia").val();
    var uRL = window.location.hostname;
    console.log(id_colonia);
    console.log(uRL);

    if (uRL == '127.0.0.1') {
        uRL = 'http://127.0.0.1:8000';
    }
    if (uRL == 'fgjem.tech') {

        uRL = 'https://fgjem.tech/sis/predenuncia';
    }

    url = uRL + "/codigo_postal/";

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_colonia": id_colonia,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            $('.cp_denunciante').attr('readonly', true);
            $(".cp_denunciante").val(data);
        }
    });
});

$(".id_colonia1").change(function() {

    var id_colonia1 = $(".id_colonia1").val();
    var currentURL = window.location.hostname;
    console.log(id_colonia1);
    if (currentURL == '127.0.0.1') {
        currentURL = 'http://127.0.0.1:8000';
    }
    if (currentURL == 'fgjem.tech') {

        currentURL = 'https://fgjem.tech/sis/predenuncia';
    }
    url = currentURL + "/codigo_postal/";

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_colonia": id_colonia1,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            $('#cp_hechos').attr('readonly', true);
            $("#cp_hechos").val(data);
        }
    });
});