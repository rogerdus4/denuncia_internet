$(document).ready(function() {

    var id_municipio = localStorage.getItem('id_municipio1');
    var localidad_hechos = localStorage.getItem('localidad_hechos');

    if (id_municipio != null) {

        var cURL = window.location.hostname;

        if (cURL == '127.0.0.1') {
            cURL = 'http://127.0.0.1:8000';
        }

        if (cURL == 'fgjem.tech') {
            cURL = 'https://fgjem.tech/sis/predenuncia';
        }
        url = cURL + "/combo_loc/";

        $.ajax({
            url: url,
            type: 'post',
            data: {
                "id_municipio": id_municipio,
                "_token": $("meta[name='csrf-token']").attr("content")
            },
            success: function(data) {
                $("#localidad_hechos").html(llenaComboLocalidad(data));
                if (localidad_hechos != null) {
                    $("#localidad_hechos").val(localidad_hechos);
                }
                quitar_required(data);
            }
        });
    }

});

$("#id_municipio1").change(function() {
    var id_municipio = $("#id_municipio1").val();

    var cURL = window.location.hostname;
    if (cURL == '127.0.0.1') {
        cURL = 'http://127.0.0.1:8000';
    }
    if (cURL == 'fgjem.tech') {
        cURL = 'https://fgjem.tech/sis/predenuncia';
    }
    url = cURL + "/combo_loc/";

    $.ajax({
        url: url,
        type: 'post',
        data: {
            "id_municipio": id_municipio,
            "_token": $("meta[name='csrf-token']").attr("content")
        },
        success: function(data) {
            $("#localidad_hechos").html(llenaComboLocalidad(data));
            localStorage.setItem('id_municipio1', id_municipio);
            quitar_required(data);
        }
    });


});

$("#localidad_hechos").change(function() {
    var localidad_hechos = $("#localidad_hechos").val();
    localStorage.setItem('localidad_hechos', localidad_hechos);
});

function llenaComboLocalidad(data) {
    var sm;
    sm += '<option value="">Seleccione</option>';
    for (var i in data) {
        sm += "<option value='" + data[i].id_localidades + "'>" + data[i].nombre + "</option>";
    }
    return sm;
}

function quitar_required(data) {
    if (data.length <= 0) {
        $("#localidad_hechos").removeAttr('aria-required');
        $("#localidad_hechos").removeAttr('required');
    } else {
        $("#localidad_hechos").attr('aria-required');
        $("#localidad_hechos").attr('required');
    }
}