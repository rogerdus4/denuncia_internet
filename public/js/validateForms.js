$(document).ready(() => {
    $("#apellido_paterno").focusout(function() {
        if ($("#apellido_paterno").val().length != 0) {
            $("#apellido_paterno").prop("required", false);
            $("#apellido_materno").prop("required", false);
        } else {
            $("#apellido_paterno").prop("required", false);
            $("#apellido_materno").prop("required", true);

        }

    });
    $("#apellido_materno").focusout(function() {
        if ($("#apellido_materno").val().length != 0) {
            $("#apellido_paterno").prop("required", false);
            $("#apellido_materno").prop("required", false);
        } else {
            $("#apellido_materno").prop("required", false);
            $("#apellido_paterno").prop("required", true);
        }
    });
});

$(document).ready(function() {
    $('#descripcion').keyup(updateCount);
    $('#descripcion').keydown(updateCount);

    function updateCount() {
        let maxLength = 4000;
        let charsEntered = $(this).val().length;
        $('.right-side-count').text(maxLength - charsEntered);
    }
});