<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoObjetos extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_tobjeto';

    protected $fillable = ['id_tobjeto','tipo_objeto'];
}
