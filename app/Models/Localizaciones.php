<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Localizaciones extends Model {
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_localizacion';

    protected $fillable =
    [
        'id_localizacion',
        'id_estado',
        'id_municipio',
        'id_colonia',
        'calle',
        'num_int',
        'num_ext',
        'referencia_1',
        'referencia_2',
        'modelo_id',
        'modelo_type',
    ];

    protected $table = 'localizaciones';

    public function modelo() {
        return $this->morphto();
    }

    public function estado(){
        return $this->belongsTo(Estados::class,'id_estado');
    }

    public function municipio(){
        return $this->belongsTo(Municipios::class,'id_municipio');
    }

    public function colonia(){
        return $this->belongsTo(Colonias::class,'id_colonia');
    }
}
