<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Denuncias extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_denuncia';
    protected $table = 'denuncias';
    protected $fillable =
    [
        'fecha_denuncia',
        'medio_adjunto',
        'observaciones',
        'id_persona',
        'id_hechos'
    ];

    public function persona(){
        return $this->belongsTo(Personas::class,'id_persona','id_persona');
    }

    public function hecho(){
        return $this->belongsTo(Hechos::class,'id_hechos','id_hechos');
    }

}
