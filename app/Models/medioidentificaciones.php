<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class medioidentificaciones extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_identificacion';
    protected $fillable = ['id_identificacion', 'identificacion'];

}
