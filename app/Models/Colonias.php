<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Colonias extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_colonia';
    protected $fillable = ['id_colonia', 'colonia','id_municipio'];
}
