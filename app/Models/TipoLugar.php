<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoLugar extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_tlugar';

    protected $fillable = [
        'tipo_lugar',
    ];

    protected $table = 'tipo_lugar';
}
