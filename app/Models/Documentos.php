<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documentos extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'documentos';
    protected $primaryKey = 'id_documentos';

    protected $fillable =
    [
        'especificaciones',
        'id_denuncia',
        'id_tdocumento',
    ];

    public function tipoDocumento()
    {
        return $this->belongsTo(TipoDocumento::class, 'id_tdocumento', 'id_tdocumento');
    }

    public function denuncia()
    {
        return $this->belongsTo(Denuncias::class,'id_denuncia','id_denuncia');
    }
}
