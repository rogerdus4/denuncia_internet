<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hechos extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_hechos';

    protected $fillable = [
        'hechos',
        'fecha_hechos',
        'hora_hechos',
        'violencia',
        'id_tlugar',
    ];

    protected $table = 'hechos';

    public function tipoLugar()
    {
        return $this->belongsTo(TipoLugar::class, 'id_tlugar', 'id_tlugar');
    }

    public function lugarHechos(){
        return $this->morphOne('App\Models\Localizaciones','modelo');
    }

}
