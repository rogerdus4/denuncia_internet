<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDocumentos extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_tdocumento';
    protected $table = 'tipo_documentos';
    protected $filliable = ['id_tdocumento', 'descripcion'];


    public function tipoDocumento(){
        return $this->belongsTo(TipoDocumento::class,'id_tdocumento','id_tdocumento');
    }
}
