<?php

namespace App\Models;

use Database\Seeders\TipoDocumento;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Objetos extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_objeto';
    protected $table = 'objetos';

    protected $fillable =
    [
        'descripcion',
        'marca',
        'modelo',
        'costo',
        'compania',
        'id_tobjeto',
        'id_denuncia',
    ];

    public function tipoObjeto(){
        return $this->belongsTo(TipoObjetos::class,'id_tobjeto','id_tobjeto');
    }

    public function denuncia()
    {
        return $this->belongsTo(Denuncias::class,'id_denuncia','id_denuncia');
    }

}
