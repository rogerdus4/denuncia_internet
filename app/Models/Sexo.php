<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sexo extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_sexo';
    protected $fillable = ['id_sexo','sexo'];

}
