<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Personas extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_persona';

    protected $fillable = [
        'nom_persona',
        'app_persona',
        'apm_persona',
        'id_sexo',
        'telefono',
        'correo',
        'id_estado_civil',
        'id_identificacion',
        'no_identificacion',
        'edad',
        'fecha_nacimiento',
        'rfc',
        'curp',
        'id_ocupacion',
    ];

    protected $table = 'personas';

    public function estadoCivil()
    {
        return $this->belongsTo(EstadosCiviles::class, 'id_estado_civil', 'id_estado_civil');
    }

    public function sexo ()
    {
        return $this->belongsTo(Sexo::class, 'id_sexo', 'id_sexo');
    }

    public function ocupacion ()
    {
        return $this->belongsTo(Ocupaciones::class, 'id_ocupacion', 'id_ocupacion');
    }

    public function identificacion()
    {
        return $this->belongsTo(medioidentificaciones::class, 'id_identificacion', 'id_identificacion');
    }

    public function domicilio()
    {
        return $this->morphOne('App\Models\Localizaciones', 'modelo');
    }


}
