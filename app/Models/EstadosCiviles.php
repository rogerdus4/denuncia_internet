<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EstadosCiviles extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_estado_civil';
    protected $fillable = ['id_estado_civil','descripcion'];
}
