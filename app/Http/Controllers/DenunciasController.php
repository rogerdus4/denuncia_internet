<?php

namespace App\Http\Controllers;

use App\Models\Denuncias;
use Illuminate\Http\Request;
use PDF;

class DenunciasController extends Controller
{
    public function generaPdf($id)
    {
        $denuncia = Denuncias::with('persona')->with('hecho')->where('id_denuncia',$id)->firstOrFail();

        // return view('denuncias.pdf-denuncia',compact('denuncia'));

        //$pdf = PDF::loadView('denuncias.pdf-denuncia', ['denuncia'=>$denuncia]);

        return PDF::loadView('denuncias.pdf-denuncia', ['denuncia' => $denuncia])
        ->stream('archivo.pdf');
        return $pdf->download('archivo.pdf');

    }
}
