<?php

namespace App\Http\Livewire\DenunciaUsuario;

use App\Models\Denuncias;
use Livewire\Component;

class MuestraDenuncia extends Component
{
    public $id_denuncia = '';
    public $denuncia;



    public function mount($id)

    {
        $this->denuncia = Denuncias::with('persona')->with('hecho')->where('id_denuncia',$id)->firstOrFail();
    }

    public function render()
    {
        return view('livewire.denuncia-usuario.muestra-denuncia', [
            'denuncia' => $this->denuncia,
        ]);
    }

}
