<?php

namespace App\Http\Livewire\DenunciaUsuario;

use Livewire\Component;
use App\Models\Estados;
use App\Models\Municipios;
use App\Models\Colonias;
use App\Models\Denuncias;
use App\Models\Documentos;
use App\Models\medioidentificaciones;
use App\Models\Sexo;
use App\Models\Ocupaciones;
use App\Models\EstadosCiviles;
use App\Models\Hechos;
use App\Models\Objetos;
use App\Models\TipoObjetos;
use App\Models\Personas;
use App\Models\TipoDocumentos;
use App\Models\TipoLugar;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class Denunciainternet extends Component
{
    public $municipios = null, $colonias = null;
    public $munhechos = null, $colhechos = null;
    public $app_persona, $apm_persona, $nom_persona, $id_identificacion;
    public $no_identificacion, $id_sexo, $id_estado_civil, $fecha_nacimiento;
    public $id_ocupacion,$rfc,$curp,$correo,$medio_adjunto;
    public $selectedEstado = null, $selectedMunicipio = null, $selectedColonia = null;
    public $calle, $ext, $int, $cp, $telefono,$fecha_hechos, $hora_hechos ,$hechos;
    public $selectedEdoHechos = null, $selectedMunHechos = null, $selectedColHechos = null;
    public $calle_hechos, $ext_hechos, $int_hechos, $lugar_hechos, $referencia_1, $referencia_2,$id_tlugar;
    public $selObjetos = [];
    public $tobjetos = [];
    public $tdocumentos = [];
    public $selDocumentos = [];
    public $currentStep = 1;
    public $totalSteps = 5;
    public $datosGenerales = [];
    public $datosDomicilio = [];
    public $datosHechos = [];
    public $localizacionHechos = [];

    public function mount()
    {
        $this->currentStep = 1;
        $this->tobjetos = TipoObjetos::all();
        $this->tdocumentos = TipoDocumentos::all();

        $this->selObjetos = [
            ['objeto_id' => '',
                'compania' => '',
                'marca' => '',
                'modelo' => '',
                'costo' => ''
             ]
        ];
        $this->selDocumentos = [
                [
                    'documento_id' =>'',
                    'especificaciones' => '',
                ]
            ];
    }

    public function render()
    {
        /**Mantiene actualizado el valor del array con cualquier cambio que suceda */
        info($this->selObjetos);
        info($this->selDocumentos);

        return view('livewire.denuncia-usuario.denunciainternet',
        [
            'estados' => Estados::all(),
            'medioidentificacion' => medioidentificaciones::all(),
            'sexos' => Sexo::all(),
            'ocupaciones' => Ocupaciones::all(),
            'estado_civil' => EstadosCiviles::all(),
            'edohechos' => Estados::where('id_estado',15)->get(),
            'tipoLugares' => TipoLugar::all(),
        ]);
    }

    public function store()
    {
        $this->buildArray();

        try {
            DB::beginTransaction();
            $persona = Personas::create($this->datosGenerales);
            $persona->domicilio()->create($this->datosDomicilio);

            $hechos= Hechos::create($this->datosHechos);
            $hechos->lugarHechos()->create($this->localizacionHechos);

            $denuncia = Denuncias::create([
                'fecha_denuncia' => Carbon::now(),
                'observaciones' => 'prueba de observaciones',
                'id_persona' => $persona->id_persona,
                'id_hechos' => $hechos->id_hechos,
                'medio_adjunto' => 'muestra',
            ]);

            foreach($this->selDocumentos as $document)
            {

                if(isset($document['documento_id']))
                {
                    Documentos::create(
                        [
                            'especificaciones' => $document['especificaciones'],
                            'id_denuncia' => $denuncia->id_denuncia,
                            'id_tdocumento' => $document['documento_id']
                        ]);
                }
            }

            foreach($this->selObjetos as $objeto)
            {
                if(isset($objeto['objeto_id']))
                {
                    Objetos::create(
                        [
                            'descripcion' => '',
                            'marca' => $objeto['marca'],
                            'modelo' => $objeto['modelo'],
                            'costo' => $objeto['costo'],
                            'compania' => $objeto['compania'],
                            'id_tobjeto' => $objeto['objeto_id'],
                            'id_denuncia' => $denuncia->id_denuncia,
                        ]);
                }
            }

            DB::commit();

            return redirect()->route('muestraConstancia', ['id' => $denuncia->id_denuncia]);


        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }

    }

    public function validateData(){

        if($this->currentStep == 1){
            $this->validate(
                [
                    'app_persona' => ['required', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:2', 'max:100'],
                    'apm_persona' => ['nullable', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u'],
                    'nom_persona' => ['required', 'regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/u', 'min:2', 'max:100'],
                    'curp' => ['required','max:18'],
                    'rfc' => ['max:13'],
                    'id_identificacion' =>'required',
                    'no_identificacion' => 'required',
                    'id_sexo' => 'required',
                    'id_estado_civil' => 'required',
                    'fecha_nacimiento' => 'required',
                    'id_ocupacion' => 'required',
                    'telefono' => ['max:10'],
                    'correo' => 'nullable|email',
                    //'medio_adjunto' => ['nullable','mimes:jpeg,png,jpg,gif,svg,png', 'max:25000'],
                ]
            );
        }
        elseif($this->currentStep == 2)
        {
             $this->validate(
                    [
                        'selectedEstado' => 'required',
                        'selectedMunicipio' => 'required',
                        'selectedColonia' => 'required',
                        'calle' => 'required',
                        'ext' => 'required',
                        'cp' => 'required|digits:5',
                    ]
            );
        }elseif($this->currentStep == 3)
        {
             $this->validate(
                [
                    'fecha_hechos' => 'required|date',
                    'hora_hechos' => 'required',
                    'selectedEdoHechos' => 'required',
                    'selectedMunHechos' => 'required',
                    'selectedColHechos' => 'required',
                    'hechos' => ['required', 'min:20', 'max:500'],
                    'id_tlugar' => 'required'
                ]);
        }

    }


    public function updatedselectedEstado($id_estado)
    {
        $this->municipios = Municipios::where('id_estado', $id_estado)
                                        ->orderBY('municipio','asc')
                                        ->get();
    }

    public function updatedselectedMunicipio($id_municipio)
    {
        $this->colonias = Colonias::where('id_municipio', $id_municipio)
                                    ->orderBy('colonia','asc')
                                    ->get();
    }

    public function updatedselectedEdoHechos($id_edohechos)
    {
        $this->munhechos = Municipios::where('id_estado', $id_edohechos)
                                        ->orderBY('municipio','asc')
                                        ->get();
    }

    public function updatedselectedMunHechos($id_munhechos)
    {
        $this->colhechos = Colonias::where('id_municipio',$id_munhechos)
                                    ->orderBy('colonia','asc')
                                    ->get();

    }

    /** Agrega un objeto a la lista */
    public function addObjeto()
    {
        $this->selObjetos[] =
        [
            'objeto_id' => '',
            'compania' => '',
            'marca' => '',
            'modelo' => '',
            'costo' => ''
        ];

    }

    public function addDocumento()
    {
        $this->selDocumentos[] =
        [
            'documento_id' => '',
            'especificaciones' => ''
        ];
    }

    /** Funcion que remueve de la lista el objeto seleccionado */
    public function removeObjeto($index)
    {
        unset($this->selObjetos[$index]);
        $this->selObjetos = array_values($this->selObjetos);
    }

    public function removeDocumento($idx)
    {
        unset($this->selDocumentos[$idx]);
        $this->selDocumentos = array_values($this->selDocumentos);
    }

    public function increaseStep(){
        $this->resetErrorBag();
        $this->validateData();
         $this->currentStep++;
         if($this->currentStep > $this->totalSteps){
             $this->currentStep = $this->totalSteps;
         }
    }

    public function decreaseStep(){
        $this->resetErrorBag();
        $this->currentStep--;
        if($this->currentStep < 1){
            $this->currentStep = 1;
        }
    }

    public function buildArray()
    {
        $this->datosGenerales =
        [
            'nom_persona' => $this->nom_persona,
            'app_persona' => $this->app_persona,
            'apm_persona' => $this->apm_persona,
            'id_sexo' => $this->id_sexo,
            'telefono' => $this->telefono,
            'correo' => $this->correo,
            'id_estado_civil' => $this->id_estado_civil,
            'id_identificacion' => $this->id_identificacion,
            'no_identificacion' => $this->no_identificacion,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'rfc' => $this->rfc,
            'curp' => $this->curp,
            'id_ocupacion' => $this->id_ocupacion,
        ];

        $this->datosDomicilio =
        [
            'id_estado' => $this->selectedEstado,
            'id_municipio' => $this->selectedMunicipio,
            'id_colonia' => $this->selectedColonia,
            'calle' => $this->calle,
            'num_ext' => $this->ext,
            'num_int' => $this->int,
        ];

        $this->datosHechos =
        [
            'hechos' => $this->hechos,
            'fecha_hechos' => $this->fecha_hechos,
            'hora_hechos' => $this->hora_hechos,
            'id_tlugar' => $this->id_tlugar,
        ];

        $this->localizacionHechos =
        [
            'id_estado' => $this->selectedEdoHechos,
            'id_municipio' => $this->selectedMunHechos,
            'id_colonia' => $this->selectedColHechos,
            'calle' => $this->calle_hechos,
            'num_ext' => $this->ext_hechos,
            'num_int' => $this->int_hechos,
            'referencia_1' => $this->referencia_1,
            'referencia_2' => $this->referencia_2,
        ];

    }

    protected $messages = [

        'app_persona.required' => 'El Apellido Paterno es obligatorio.',
        'app_persona.regex' => 'El Apellido Paterno debe  contener solo letras.',
        'app_persona.min' => 'El Apellido Paterno debe  contener mas de una letra.',
        'app_persona.max' => 'El Apellido Paterno debe  contener menos de 100 caracteres.',
        'apm_persona.regex' => 'El Apellido Materno debe  contener solo letras.',
        'nom_persona.required' => 'El Nombre es obligatorio.',
        'nom_persona.regex' => 'El Nombre debe  contener solo letras.',
        'nom_persona.min' => 'El Nombre debe  contener mas de una letra.',
        'nom_persona.max' => 'El Nombre debe  contener menos de 100 caracteres.',
        'no_identificacion.required' => 'El Num. de Identificación es obligatorio.',
        'id_identificacion.required' => 'El Medio de Identificación es obligatorio.',
        'id_sexo.required' => 'El Sexo es obligatorio.',
        'id_estado_civil.required' => 'El Estado Civil es obligatorio.',
        'fecha_nacimiento.required' => 'La fecha de Nacimiento es obligatoria.',
        'id_ocupacion.required' => 'La ocupación es obligatorio.',
        'correo.email' => 'Debe ser un correo valido.',
        'medio_adjunto.required' => 'La ocupación es obligatoria.',
        'medio_adjunto.mimes' => 'El Adjuto con el que se identifica debe ser jpg,jpeg,png,gif.',
        'medio_adjunto.max' => 'El Adjuto con el que se identifica debe pesar maximo 250Kb.',
        'selectedEstado.required' => 'Seleccione un Estado.',
        'selectedMunicipio.required' => 'Seleccione un Municipio.',
        'selectedColonia.required' => 'Seleccione una Colonia.',
        'calle.required' => 'La Calle es obligatoria.',
        'ext.required' => 'El Numero Exterior es obligatorio.',
        'cp.required' => 'El Codigo Postal es obligatorio.',
        'cp.digits' => 'Maximo 5 digitos.',
        'fecha_hechos.required' => 'La Fecha de Hechos es obligatoria.',
        'hora_hechos.required' => 'La Hora de los hechos es obligatorio.',
        'selectedEdoHechos.required' => 'El Estado es obligatorio.',
        'selectedMunHechos.required' => 'El Municipio es obligatorio.',
        'selectedColHechos.required' => 'La Calle es obligatoria.',
        'lugar_hechos.required' => 'El lugar de hechos es obligatorio.',
        'hechos.required' => 'La Narración de hechos es obligatorio.',
        'curp.max' => 'El CURP debe  contener menos de 18 caracteres.',
        'curp.required' => 'El CURP es obligatorio.',
        'rfc.max' => 'El RFC debe  contener menos de 13 caracteres.',
        'id_tlugar' => 'El Punto de Referencia es obligatorio'
    ];


}

