<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Livewire\WithPagination;

class General extends Component
{

    public $f_inicial;
    public $f_final;

    public function render()
    {

        $denuncias = DB::table('denuncias')
            ->join('documentos', 'denuncias.id_denuncia', '=', 'documentos.id_denuncia')
            ->join('objetos', 'denuncias.id_denuncia', '=', 'objetos.id_denuncia')
            //->join('localizaciones', 'users.id', '=', 'orders.user_id')
            ->join('personas', 'denuncias.id_persona', '=', 'personas.id_persona')
            ->join('hechos', 'denuncias.id_hechos', '=', 'denuncias.id_hechos')
            ->whereBetween('fecha_denuncia', [$this->f_inicial, $this->f_final])
            ->paginate(10);

           // dd($denuncias);

        return view('livewire.admin.general',
               compact("denuncias"))->layout('layouts.admin');
    }
}
