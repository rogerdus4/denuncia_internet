<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Reportes extends Component
{

    public $filtros= null;
    public $f_inicial;
    public $f_final;
    public $data;
    public $fil;

    protected $rules = [
        'filtros' => 'required',
        'f_inicial' => 'required',
        'f_final' => 'required',
    ];

    protected $messages = [
        'filtros.required' => 'Debe seleccionar al menos un filtro',
        'f_inicial.required' => 'Seleccione una fecha inicial válida',
        'f_final.required' => 'Seleccione una fecha final válida'
    ];

    public function submit()
    {
        $validatedData = $this->validate();

        // Add registration data to modal
        //dd( $validatedData );
    }

    public function arreglo_municipios(){

        $r_municipio = DB::table('denuncias')
        ->select('*')
        ->join('localizaciones', 'denuncias.id_denuncia', '=', 'localizaciones.model_id')
        ->join('municipios', 'municipios.id_municipio', '=', 'localizaciones.id_municipio')
        ->whereBetween('fecha_denuncia', [$this->f_inicial, $this->f_final])
        ->paginate(10);

        $arreglo = [];
        $data_municipio = [];
        $robo=0;
        $extravio=0;

        //listamos todos los municipios
        foreach ($r_municipio as $municipio) {
                    $data_municipio = $municipio->municipio;
        }

        //filtramos los municipios unicos
        $data_municipio = array_unique($data_municipio);

        //buscamos la clave de cada municipio
        foreach ($r_municipio as $municipio) {
            foreach($data_municipio as $d_municipio){

                if($municipio->municipio==$d_municipio->municipio){
                    if($municipio->tipo_denuncia==1){
                        $extravio++;
                    }else{
                        $robo++;
                    }

                    $arreglo[$data_municipio]["robo"] = $robo;
                    $arreglo[$data_municipio]["extravio"] = $extravio;
                }
            }
        }

        //dd($arreglo);


        return $arreglo;
    }


    public function render()
    {
        $r_delito = DB::table('denuncias')
        ->select('tipo_denuncia')
        ->whereBetween('fecha_denuncia', [$this->f_inicial, $this->f_final])
        ->paginate(10);

        $r_genero = DB::table('denuncias')
        ->select('sexo')
        ->join('personas', 'denuncias.id_persona', '=', 'personas.id_persona')
        ->whereBetween('fecha_denuncia', [$this->f_inicial, $this->f_final])
        ->paginate(10);

        $r_municipio = $this->arreglo_municipios();

        $r_documento = DB::table('denuncias')
        ->select('tipo_documentos.descripcion')
        ->join('documentos', 'documentos.id_denuncia', '=', 'denuncias.id_denuncia')
        ->join('tipo_documentos', 'tipo_documentos.id_tdocumento', '=', 'documentos.id_tdocumento')
        ->whereBetween('fecha_denuncia', [$this->f_inicial, $this->f_final])
        ->paginate(10);

        $r_objeto = DB::table('denuncias')
        ->select('objetos.descripcion')
        ->join('objetos', 'objetos.id_denuncia', '=', 'denuncias.id_denuncia')
        ->whereBetween('fecha_denuncia', [$this->f_inicial, $this->f_final])
        ->paginate(10);

        $r_general = DB::table('denuncias')
        ->select('objetos.descripcion')
        ->join('objetos', 'objetos.id_denuncia', '=', 'denuncias.id_denuncia')
        ->whereBetween('fecha_denuncia', [$this->f_inicial, $this->f_final])
        ->paginate(10);

        return view('livewire.admin.reportes',  compact("r_delito",
                                                        "r_genero",
                                                        "r_municipio",
                                                        "r_documento",
                                                        "r_objeto",
                                                        "r_general"))
                                                ->layout('layouts.admin');
    }
}
