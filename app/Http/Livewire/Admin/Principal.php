<?php

namespace App\Http\Livewire\Admin;


use Livewire\WithPagination;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Principal extends Component
{

    use WithPagination;

    public $c_folio;

    protected $rules = [
        'c_folio' => 'required|min:6'
    ];

    public function render()
    {
        $denuncias = DB::table('denuncias')
        ->join('documentos', 'denuncias.id_denuncia', '=', 'documentos.id_denuncia')
        ->join('objetos', 'denuncias.id_denuncia', '=', 'objetos.id_denuncia')
        //->join('localizaciones', 'users.id', '=', 'orders.user_id')
        ->join('personas', 'denuncias.id_persona', '=', 'personas.id_persona')
        ->join('hechos', 'denuncias.id_hechos', '=', 'denuncias.id_hechos')
        ->where('denuncias.id_denuncia', '=', $this->c_folio)
        ->paginate(10);
        return view('livewire.admin.principal', compact("denuncias"))->layout('layouts.admin');
    }

    public function submit()
    {
        // $denuncias = Denuncias::orderBy('id_denuncia', 'desc')->paginate(10);
        // return view('livewire.admin.consulta-folio-individual', compact('denuncias'))->layout('layouts.admin');
    }
}
