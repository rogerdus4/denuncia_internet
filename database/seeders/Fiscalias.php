<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use Illuminate\Support\Facades\DB;

class fiscalias extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fiscalias')->insert(['id_fiscalia' => 1 ,'fiscalia' =>'Fiscalía Regional de Ecatepec de Morelos']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 2 ,'fiscalia' =>'Fiscalía Regional de Tlalnepantla de Baz']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 3 ,'fiscalia' =>'Fiscalía Regional de Cuautitlán Izcalli']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 4 ,'fiscalia' =>'Fiscalía Regional de Nezahualcóyotl']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 5 ,'fiscalia' =>'Fiscalía Regional de Amecameca']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 6 ,'fiscalia' =>'Fiscalía Regional de Texcoco de Mora']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 7 ,'fiscalia' =>'Fiscalía Regional de Toluca']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 8 ,'fiscalia' =>'Fiscalía Regional de Atlacomulco']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 9 ,'fiscalia' =>'Fiscalía Regional de Valle de Bravo']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 10 ,'fiscalia' =>'Fiscalía Regional de Tejupilco']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 11 ,'fiscalia' =>'Fiscalía Regional de Ixtapan de la Sal']);
        DB::table('fiscalias')->insert(['id_fiscalia' => 9999 ,'fiscalia' =>'Sin Fiscalia']);
    }
}
