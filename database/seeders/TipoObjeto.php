<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoObjeto extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 1 ,'tipo_objeto' =>'Teléfono']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 2 ,'tipo_objeto' =>'Aparatos eletrónicos o electrodomésticos']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 3 ,'tipo_objeto' =>'Cámara fotográfica']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 4 ,'tipo_objeto' =>'Cámara de video']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 5 ,'tipo_objeto' =>'Otros objetos']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 6 ,'tipo_objeto' =>'Computadora']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 7 ,'tipo_objeto' =>'Consola de Videojuegos']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 9 ,'tipo_objeto' =>'Estereo']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 10 ,'tipo_objeto' =>'Estufa']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 11 ,'tipo_objeto' =>'Ipad']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 12 ,'tipo_objeto' =>'Ipod']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 13 ,'tipo_objeto' =>'Lavadora']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 14 ,'tipo_objeto' =>'Licuadora']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 15 ,'tipo_objeto' =>'Microondas']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 16 ,'tipo_objeto' =>'Refrigerador']);
        DB::table('tipo_objetos')->insert(['id_tobjeto' => 17 ,'tipo_objeto' =>'Televisor']);
    }
}
