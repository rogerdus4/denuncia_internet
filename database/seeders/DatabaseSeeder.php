<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(EstadosSeeder::class);
        $this->call(Fiscalias::class);
        $this->call(Municipios::class);
        $this->call(TipoObjeto::class);
        $this->call(TipoDocumento::class);
        $this->call(EstadoCivilSeeder::class);
        $this->call(OcupacionesSeeder::class);
        $this->call(TipoLugarSeeder::class);
        $this->call(SexoSeeder::class);
        $this->call(medioidentificaciones::class);
        $this->call(ColoniasSeeder::class);

    }
}
