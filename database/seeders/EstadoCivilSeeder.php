<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados_civiles')->insert([
            'id_estado_civil' => 1,
            'descripcion' => 'SOLTERO(A)'
        ]);

        DB::table('estados_civiles')->insert([
            'id_estado_civil' => 2,
            'descripcion' => 'CASADO(A)'
        ]);
    }
}
