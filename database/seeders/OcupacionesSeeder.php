<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class OcupacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 1,
            'ocupacion' => 'Profesionista']);

        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 2,
            'ocupacion' => 'Jubilado(a)']);

        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 3,
            'ocupacion' => 'Pensionado(a)']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 4,
            'ocupacion' => 'Empleado(a)']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 5,
            'ocupacion' => 'Chofer']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 6,
            'ocupacion' => 'Empresario(a)']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 7,
            'ocupacion' => 'Desempleado(a)']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 8,
            'ocupacion' => 'Hogar']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 9,
            'ocupacion' => 'Servidor Público']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 10,
            'ocupacion' => 'Comerciante']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 11,
            'ocupacion' => 'Artista']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 12,
            'ocupacion' => 'Cantante']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 13,
            'ocupacion' => 'Campesino']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 14,
            'ocupacion' => 'Estudiante']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 15,
            'ocupacion' => 'Obrero']);
        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 16,
            'ocupacion' => 'Vendedor Ambulante']);

        DB::table('ocupaciones')->insert([
            'id_ocupacion' => 9999,
            'ocupacion' => 'No especificado']);
    }
}
