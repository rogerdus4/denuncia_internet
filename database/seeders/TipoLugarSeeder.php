<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TipoLugarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('tipo_lugar')->insert([
             'id_tlugar' => 1 ,
             'tipo_lugar' => 'Campo de Terraceria'
         ]);
         DB::table('tipo_lugar')->insert([
            'id_tlugar' => 2,
            'tipo_lugar' => 'Casa Habitacion'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 3,
            'tipo_lugar' => 'Centro de Trabajo'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 4,
            'tipo_lugar' => 'Centro Deportivo'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 5,
            'tipo_lugar' => 'Hospital'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 6,
            'tipo_lugar' => 'Hotel'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 7,
            'tipo_lugar' => 'Inmueble en construccion'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 8,
            'tipo_lugar' => 'Negocio'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 9,
            'tipo_lugar' => 'Paraje Solitario'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 10,
            'tipo_lugar' => 'Reclusorio'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 11,
            'tipo_lugar' => 'Rio de Aguas Negras'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 12,
            'tipo_lugar' => 'Terreno Baldio'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 13,
            'tipo_lugar' => 'Tianguis'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 14,
            'tipo_lugar' => 'Via publica'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 15,
            'tipo_lugar' => 'Desconocido'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 16,
            'tipo_lugar' => 'Centro Comercial'
        ]);
        DB::table('tipo_lugar')->insert([
            'id_tlugar' => 9999,
            'tipo_lugar' => 'No especificado'
        ]);
    }
}
