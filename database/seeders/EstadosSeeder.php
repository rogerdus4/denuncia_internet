<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert(['id_estado' =>1,'estado' => 'Aguascalientes']);
        DB::table('estados')->insert(['id_estado' =>2,'estado' => 'Baja California']);
        DB::table('estados')->insert(['id_estado' =>3,'estado' => 'Baja California Sur']);
        DB::table('estados')->insert(['id_estado' =>4,'estado' => 'Campeche']);
        DB::table('estados')->insert(['id_estado' =>7,'estado' => 'Chiapas']);
        DB::table('estados')->insert(['id_estado' =>8,'estado' => 'Chihuahua']);
        DB::table('estados')->insert(['id_estado' =>9,'estado' => 'Ciudad de México']);
        DB::table('estados')->insert(['id_estado' =>5,'estado' => 'Coahuila de Zaragoza']);
        DB::table('estados')->insert(['id_estado' =>6,'estado' => 'Colima']);
        DB::table('estados')->insert(['id_estado' =>10,'estado' => 'Durango']);
        DB::table('estados')->insert(['id_estado' =>11,'estado' => 'Guanajuato']);
        DB::table('estados')->insert(['id_estado' =>12,'estado' => 'Guerrero']);
        DB::table('estados')->insert(['id_estado' =>13,'estado' => 'Hidalgo']);
        DB::table('estados')->insert(['id_estado' =>14,'estado' => 'Jalisco']);
        DB::table('estados')->insert(['id_estado' =>16,'estado' => 'Michoacán de Ocampo']);
        DB::table('estados')->insert(['id_estado' =>17,'estado' => 'Morelos']);
        DB::table('estados')->insert(['id_estado' =>15,'estado' => 'México']);
        DB::table('estados')->insert(['id_estado' =>18,'estado' => 'Nayarit']);
        DB::table('estados')->insert(['id_estado' =>19,'estado' => 'Nuevo León']);
        DB::table('estados')->insert(['id_estado' =>20,'estado' => 'Oaxaca']);
        DB::table('estados')->insert(['id_estado' =>21,'estado' => 'Puebla']);
        DB::table('estados')->insert(['id_estado' =>22,'estado' => 'Querétaro']);
        DB::table('estados')->insert(['id_estado' =>23,'estado' => 'Quintana Roo']);
        DB::table('estados')->insert(['id_estado' =>24,'estado' => 'San Luis Potosí']);
        DB::table('estados')->insert(['id_estado' =>25,'estado' => 'Sinaloa']);
        DB::table('estados')->insert(['id_estado' =>26,'estado' => 'Sonora']);
        DB::table('estados')->insert(['id_estado' =>27,'estado' => 'Tabasco']);
        DB::table('estados')->insert(['id_estado' =>28,'estado' => 'Tamaulipas']);
        DB::table('estados')->insert(['id_estado' =>29,'estado' => 'Tlaxcala']);
        DB::table('estados')->insert(['id_estado' =>30,'estado' => 'Veracruz de Ignacio de la Llave']);
        DB::table('estados')->insert(['id_estado' =>31,'estado' => 'Yucatán']);
        DB::table('estados')->insert(['id_estado' =>32,'estado' => 'Zacatecas']);
    }
}
