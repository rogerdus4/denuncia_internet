<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class medioidentificaciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>1,'identificacion' => 'CARTA DE NATURALIZACION']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>2,'identificacion' => 'CARTA DE PASANTE']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>3,'identificacion' => 'CARTILLA DE SERVICIO MILITAR']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>4,'identificacion' => 'CEDULA DE IDENTIFICACION CIUDADANA']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>5,'identificacion' => 'CEDULA PROFESIONAL']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>6,'identificacion' => 'CERTIFICADO DE NACIONALIDAD MEXICANA']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>7,'identificacion' => 'CERTIFICADO, CREDENCIAL ESCOLAR']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>8,'identificacion' => 'CREDENCIAL DE ELECTOR']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>9,'identificacion' => 'CREDENCIAL DE INAPAM']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>10,'identificacion' => 'CREDENCIAL DE SERVICIOS MEDICO']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>11,'identificacion' => 'CREDENCIAL DE TRABAJO DE DEPENDENCIA']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>12,'identificacion' => 'CREDENCIAL PARA JUBILADOS Y PENSIONADOS']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>13,'identificacion' => 'DECLARATORIA DE NACIONALIDAD MEXICANA']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>14,'identificacion' => 'LICENCIA DE CONDUCIR']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>15,'identificacion' => 'MATRICULA CONSULAR']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>16,'identificacion' => 'NO ESPECIFICADO']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>17,'identificacion' => 'PASAPORTE']);
        DB::table('medioidentificaciones')->insert(['id_identificacion' =>18,'identificacion' => 'TITULO PROFESIONAL']);
    }
}
