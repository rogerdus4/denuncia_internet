<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoDocumento extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 1 ,'descripcion' =>'Credencial de elector']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 2 ,'descripcion' =>'Cartilla SMN']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 3 ,'descripcion' =>'Pasaporte']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 4 ,'descripcion' =>'Cédula Profesional']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 5 ,'descripcion' =>'Licencia de conducir']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 6 ,'descripcion' =>'Gafete o credencial']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 7 ,'descripcion' =>'Actas Expedidas por el Registro Civil']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 8 ,'descripcion' =>'Actas, testimonios, copias certificadas de escrituras públicas o instrumentos notariales']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 9 ,'descripcion' =>'Facturas en original']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 10 ,'descripcion' =>'Otros documentos']);
        DB::table('tipo_documentos')->insert(['id_tdocumento' => 11 ,'descripcion' =>'Resoluciones o documentos originales, o en copia certificada, emitidos por autoridades judiciales, administrativas o del trabajo']);

    }
}
