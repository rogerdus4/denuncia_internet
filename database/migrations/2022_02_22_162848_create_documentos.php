<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->id('id_documentos');
            $table->string('especificaciones')->nullable();
            $table->unsignedBigInteger('id_denuncia');
            $table->foreign('id_denuncia')->references('id_denuncia')->on('denuncias');
            $table->unsignedBigInteger('id_tdocumento');
            $table->foreign('id_tdocumento')->references('id_tdocumento')->on('tipo_documentos');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos');
    }
}
