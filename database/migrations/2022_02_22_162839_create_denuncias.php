<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDenuncias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('denuncias', function (Blueprint $table) {
            $table->id('id_denuncia');
            $table->date('fecha_denuncia');
            $table->string('medio_adjunto',100);
            $table->longText('observaciones');
            $table->unsignedBigInteger('id_persona');
            $table->unsignedBigInteger('id_hechos');
            $table->foreign('id_persona')->references('id_persona')->on('personas');
            $table->foreign('id_hechos')->references('id_hechos')->on('hechos');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('denuncias');
    }
}
