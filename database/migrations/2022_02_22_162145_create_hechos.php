<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHechos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hechos', function (Blueprint $table) {
            $table->id('id_hechos');
            $table->longText('hechos');
            $table->date('fecha_hechos');
            $table->time('hora_hechos');
            $table->unsignedBigInteger('id_tlugar');
            $table->foreign('id_tlugar')->references('id_tlugar')->on('tipo_lugar');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hechos');
    }
}
