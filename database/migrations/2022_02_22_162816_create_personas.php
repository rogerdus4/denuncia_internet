<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sexos', function (Blueprint $table) {
            $table->id('id_sexo');
            $table->string('sexo', 100);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('personas', function (Blueprint $table) {
            $table->id('id_persona');
            $table->string('nom_persona', 100);
            $table->string('app_persona', 100);
            $table->string('apm_persona', 100);
            $table->unsignedBigInteger('id_sexo');
            $table->string('telefono');
            $table->string('correo', 65)->nullable();
            $table->unsignedBigInteger('id_estado_civil');
            $table->unsignedBigInteger('id_identificacion');
            $table->string('no_identificacion', 45);
            $table->integer('edad')->nullable();
            $table->date('fecha_nacimiento');
            $table->string('rfc', 13)->nullable();
            $table->string('curp', 18);
            $table->unsignedBigInteger('id_ocupacion');
            $table->foreign('id_estado_civil')->references('id_estado_civil')->on('estados_civiles');
            $table->foreign('id_sexo')->references('id_sexo')->on('sexos');
            $table->foreign('id_ocupacion')->references('id_ocupacion')->on('ocupaciones');
            $table->foreign('id_identificacion')->references('id_identificacion')->on('medioidentificaciones');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sexos');
        Schema::dropIfExists('personas');
    }
}
