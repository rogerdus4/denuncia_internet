<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjetos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objetos', function (Blueprint $table) {
            $table->id('id_objetos');
            $table->string('descripcion')->nullable();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->float('costo');
            $table->string('compania')->nullable();
            $table->unsignedBigInteger('id_tobjeto');
            $table->foreign('id_tobjeto')->references('id_tobjeto')->on('tipo_objetos');
            $table->unsignedBigInteger('id_denuncia');
            $table->foreign('id_denuncia')->references('id_denuncia')->on('denuncias');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objetos');
    }
}
